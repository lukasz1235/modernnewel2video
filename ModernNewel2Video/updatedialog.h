/*
    ModernNewel2Video
    Copyright (C) 2015  Lukasz Matczak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UPDATEDIALOG_H
#define UPDATEDIALOG_H

#include "updater.h"

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class UpdateDialog;
}

class UpdateDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UpdateDialog(Updater *updater, QWidget *parent = 0);
    void setCurr(QString date, int build);
    void setNew(QString date, int build);
    ~UpdateDialog();

private:
    Ui::UpdateDialog *ui;

    Updater *updater;

    QString date;
    int build;
    QString newDate;
    int newBuild;

private slots:
    void filesChecked(int size);

    void on_buttonBox_clicked(QAbstractButton *button);

signals:
    void checkFiles();
    void prepareUpdate();
};

#endif // UPDATEDIALOG_H
