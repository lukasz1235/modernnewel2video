/*
    ModernNewel2Video
    Copyright (C) 2016  Lukasz Matczak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LINUXUTILS_H
#define LINUXUTILS_H

#include "osutils.h"
#include <qtextstream.h>
#include <X11/Xlib.h>

class LinuxUtils : public OSUtils
{
   Q_OBJECT
public:
    LinuxUtils();
    ~LinuxUtils();

    QString getFriendlyName(QString dev);

    void setCursorClipGeom(QRect screen);
    void enableCursorClip(bool enabled);

    void setExtendScreenTopology();
    void setPreviousScreenTopology();

    int createThumbnail(WId srcID, QLabel *dest, bool withFrame, bool noScale);
    void setThumbnailFPS(int fps);
    void showThumbnail(int id, bool visible);
    void moveThumbnail(int id, QSize srcSize);
    void setThumbnailOpacity(int id, int opacity);
    void setThumbnailScale(int id, float scale);
    QPoint mapPointToThumbnail(int id, QPoint point);
    QPixmap getCursorForThumbnail(int id, QPoint *hotspot, bool forcePixmap);
    void destroyThumbnail(int id);

    void initPeakMeter(qint64 pid);
    void uninitPeakMeter();

    WindowInfo getWindowAt(QPoint pos, WId skipWindow);
    QString getWindowTitle(WId winID);
    QRect getWindowRect(WId winID);
    void setWindowSize(WId winID, QSize size);

    void watchMouseMove(bool watch);

    QString getDataDir();

private:
    void updateThumbnail(int id);

    QTimer updateTimer;
    int thumbnailFPS;

    Display *dpy;
    Window rootWindow;

    struct ThumbInfo
    {
        WId srcID;
        QLabel *dest;
        bool noScale;
        float scale;
        QSize srcSize;
        XImage *oldXImage;
    };

    QMap<int, ThumbInfo> thumbInfoTable;
    int currThumbInfoIdx;

    bool watchMouse;

private slots:
    void update();
};

#endif // LINUXUTILS_H
