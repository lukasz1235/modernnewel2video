/*
    ModernNewel2Video
    Copyright (C) 2015-2016  Lukasz Matczak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "sliderstyle.h"
#include "flickcharm.h"
#include "playlistmodel.h"
#include "osutils.h"
#include "mpv.h"

#include <QMainWindow>
#include <QThread>
#include <QTimer>

#ifdef Q_OS_WIN
#include <QWinTaskbarButton>
#endif

class QSettings;
class Updater;
class VideoWindow;

namespace Ui {
class MainWindow;
}

extern OSUtils *utils;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    VideoWindow *videoWindow;

private:
    void loadPlaylist();
    void addShadowEffect(QWidget *widget, QColor color);
    void calcPeakChannel(int level, QFrame *blue);
    void setTitle(QString title);
    void setCurrentPlaylistEntry(int n);
    void playFile(int n);
    void stopFile();

    bool event(QEvent *event);
    void showEvent(QShowEvent *e);
    void closeEvent(QCloseEvent *e);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void dragEnterEvent(QDragEnterEvent *e);
    void dragLeaveEvent(QDragLeaveEvent *e);
    void dragMoveEvent(QDragMoveEvent *e);
    void dropEvent(QDropEvent *e);

public slots:
    void newProcess(const QString &data);

private slots:
    void timer_event();
    void fastTimer_event();

    void onFocusChanged(QWidget *old, QWidget *now);

    void on_updateBtn_clicked();
    void on_settingsBtn_clicked();
    void on_minimizeBtn_clicked();
    void on_exitBtn_clicked();
    void on_PlayBtn_clicked();
    void on_StopBtn_clicked();
    void on_AddBtn_clicked();
    void on_AddWindowBtn_clicked();
    void on_RemoveBtn_clicked();
    void on_playOnce_clicked();
    void on_MoveUpBtn_clicked();
    void on_MoveDownBtn_clicked();
    void on_playlistView_doubleClicked(const QModelIndex &index);
    void on_PosSlider_valueChanged(int value);
    void on_PosSlider_sliderReleased();

    void showUpdate(QString version, int build);

    void setPaused(bool paused);
    void mpvEOF();
    void setCurrPos(double currPos);
    void setPeakLevel(double peakLevel);

    void screenAdded(QScreen *screen);
    void screenRemoved(QScreen *screen);
    void setScreenName(QString name);

signals:
    void checkForUpdates();

private:
    Ui::MainWindow *ui;

    QPoint movePos;
    bool canMove;

    bool paused;
    bool playOnce;

    QSettings *settings;

    QTimer timer;
    QTimer fastTimer;

    MPV *mpv;

    SliderStyle sliderStyle;

    FlickCharm flick;
    PlaylistModel playlistModel;

#ifdef Q_OS_WIN
    QThread updaterThread;
    Updater *updater;

    QSettings *globalSettings;

    QWinTaskbarButton taskbarButton;

    QString newVersion;
    int newBuild;
#endif
};

#endif // MAINWINDOW_H
