/*
    ModernNewel2Video
    Copyright (C) 2015  Lukasz Matczak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "updatedialog.h"
#include "ui_updatedialog.h"

#include <QPushButton>

UpdateDialog::UpdateDialog(Updater *updater, QWidget *parent) :
    QDialog(parent),
    updater(updater),
    ui(new Ui::UpdateDialog)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Yes)->setIcon(QApplication::style()->standardIcon(QStyle::SP_VistaShield));

    ui->buttonBox->button(QDialogButtonBox::Yes)->setText("Tak");
    ui->buttonBox->button(QDialogButtonBox::No)->setText("Nie");

    connect(this, SIGNAL(checkFiles()), updater, SLOT(checkFiles()));
    connect(this, SIGNAL(prepareUpdate()), updater, SLOT(prepareUpdate()));
    connect(updater, SIGNAL(filesChecked(int)), this, SLOT(filesChecked(int)));

    emit checkFiles();
}

UpdateDialog::~UpdateDialog()
{
    delete ui;
}

void UpdateDialog::setCurr(QString date, int build)
{
    ui->currVersion->setText("Aktualna wersja: " + date + " (build " + QString::number(build) + ")");
}

void UpdateDialog::setNew(QString date, int build)
{
    ui->newVersion->setText("Najnowsza wersja: " + date + " (build " + QString::number(build) + ")");
}

void UpdateDialog::filesChecked(int size)
{
    ui->size->setText("Rozmiar plików do pobrania: " + QLocale().toString(size/1024) + " KB");
}

void UpdateDialog::on_buttonBox_clicked(QAbstractButton *button)
{
    if(button == ui->buttonBox->button(QDialogButtonBox::Yes))
    {
        emit prepareUpdate();
    }
}
