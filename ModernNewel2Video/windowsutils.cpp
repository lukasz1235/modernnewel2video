/*
    ModernNewel2Video
    Copyright (C) 2015-2016  Lukasz Matczak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "windowsutils.h"
#include "mainwindow.h"

#include <QDebug>
#include <QScreen>
#include <QGuiApplication>
#include <QApplication>
#include <QtWin>

#include <Mmdeviceapi.h>
#include <Audiopolicy.h>

#define LOG(x,y) qDebug() << x << y;

QMap<int, WindowsUtils::ThumbInfo> WindowsUtils::thumbInfoTable;
int WindowsUtils::currThumbInfoIdx;

HHOOK mouseMoveHook;
HWINEVENTHOOK mouseChangeHook;
bool cursorChanged;

WindowsUtils::WindowsUtils()
{
    this->pAudioMeterInformation = 0;
    this->currThumbInfoIdx = 0;
    connect(&this->peakTimer, &QTimer::timeout, this, &WindowsUtils::getPeak);

    isWin10orGreater = (QSysInfo::windowsVersion() >= QSysInfo::WV_WINDOWS10);
}

WindowsUtils::~WindowsUtils()
{
    this->uninitPeakMeter(); // TODO
}

QString WindowsUtils::getFriendlyName(QString dev)
{
    UINT32 PathCount, ModeCount;
    int error = GetDisplayConfigBufferSizes(QDC_ONLY_ACTIVE_PATHS, &PathCount, &ModeCount);

    if (error != ERROR_SUCCESS)
    {
        return "";
    }

    DISPLAYCONFIG_PATH_INFO *DisplayPaths = new DISPLAYCONFIG_PATH_INFO[PathCount];
    DISPLAYCONFIG_MODE_INFO *DisplayModes = new DISPLAYCONFIG_MODE_INFO[ModeCount];
    error = QueryDisplayConfig(QDC_ONLY_ACTIVE_PATHS, &PathCount, DisplayPaths, &ModeCount, DisplayModes, NULL);

    if (error != ERROR_SUCCESS)
    {
        delete [] DisplayPaths;
        delete [] DisplayModes;
        return "";
    }

    for (UINT32 i = 0; i < PathCount; i++)
    {
        DISPLAYCONFIG_DEVICE_INFO_HEADER hdr;

        hdr.type = DISPLAYCONFIG_DEVICE_INFO_GET_SOURCE_NAME;
        hdr.id = DisplayPaths[i].sourceInfo.id;
        hdr.adapterId = DisplayPaths[i].sourceInfo.adapterId;
        hdr.size = sizeof(DISPLAYCONFIG_SOURCE_DEVICE_NAME);

        DISPLAYCONFIG_SOURCE_DEVICE_NAME srcInfo;
        memset(&srcInfo, 0, sizeof(srcInfo));
        srcInfo.header = hdr;

        DisplayConfigGetDeviceInfo(&srcInfo.header);

        if(!wcscmp(srcInfo.viewGdiDeviceName, dev.toStdWString().c_str()))
        {
            hdr.type = DISPLAYCONFIG_DEVICE_INFO_GET_TARGET_NAME;
            hdr.id = DisplayPaths[i].targetInfo.id;
            hdr.adapterId = DisplayPaths[i].targetInfo.adapterId;
            hdr.size = sizeof(DISPLAYCONFIG_TARGET_DEVICE_NAME);

            DISPLAYCONFIG_TARGET_DEVICE_NAME info;
            memset(&info, 0, sizeof(info));
            info.header = hdr;

            DisplayConfigGetDeviceInfo(&info.header);

            delete [] DisplayPaths;
            delete [] DisplayModes;

            QString ret = QString::fromUtf16((ushort*)info.monitorFriendlyDeviceName);

            if(this->getOutputTechnologyString(info.outputTechnology) != "")
                ret += " (" + this->getOutputTechnologyString(info.outputTechnology) + ")";

            return ret;
        }
    }

    delete [] DisplayPaths;
    delete [] DisplayModes;
    return "";
}

QString WindowsUtils::getOutputTechnologyString(int number)
{
    switch(number)
    {
    case 0: return "VGA";
    case 1: return "S-Video";
    case 2: return "Złącze kompozytowe";
    case 3: return "Złącze komponentowe";
    case 4: return "DVI";
    case 5: return "HDMI";
    case 6: return "LVDS";
    case 8: return "D Video";
    case 9: return "SDI";
    case 10: return "DisplayPort";
    case 11: return "DisplayPort [wewnętrzny]";
    case 12: return "UDI";
    case 13: return "UDI [wewnętrzny]";
    case 14: return "SDTV";
    case 15: return "Miracast";
    case 0x80000000: return "Ekran wewnętrzny";
    default: return "";
    }
}

void WindowsUtils::setCursorClipGeom(QRect screen)
{
    QList<QScreen*> monitors = QGuiApplication::screens();
    QRect qarea = monitors[0]->geometry();

    foreach (QScreen *monitor, monitors)
    {
        if(monitor->geometry() != screen)
        {
            qarea |= monitor->geometry();
        }
    }

    area.left = qarea.left();
    area.top = qarea.top();
    area.right = qarea.right();
    area.bottom = qarea.bottom();
}

void WindowsUtils::enableCursorClip(bool enabled)
{
    if(enabled) ClipCursor(&area);
    else ClipCursor(NULL);
}

void WindowsUtils::setExtendScreenTopology()
{
    UINT32 PathCount, ModeCount;
    int error = GetDisplayConfigBufferSizes(QDC_ONLY_ACTIVE_PATHS, &PathCount, &ModeCount);
    if (error != ERROR_SUCCESS)
    {
        return;
    }

    DISPLAYCONFIG_PATH_INFO *DisplayPaths = new DISPLAYCONFIG_PATH_INFO[PathCount];
    DISPLAYCONFIG_MODE_INFO *DisplayModes = new DISPLAYCONFIG_MODE_INFO[ModeCount];

    error = QueryDisplayConfig(QDC_DATABASE_CURRENT, &PathCount, DisplayPaths, &ModeCount, DisplayModes, &topologyId);

    SetDisplayConfig(0, NULL, 0, NULL, SDC_APPLY | SDC_TOPOLOGY_EXTEND);

    delete [] DisplayPaths;
    delete [] DisplayModes;
}

void WindowsUtils::setPreviousScreenTopology()
{
    if(topologyId == 0) return;

    SetDisplayConfig(0, NULL, 0, NULL, SDC_APPLY | topologyId);
}

void CALLBACK WinEventProc(HWINEVENTHOOK hWinEventHook, DWORD event, HWND hwnd, LONG idObject, LONG idChild, DWORD dwEventThread, DWORD dwmsEventTime)
{
    Q_UNUSED(hWinEventHook);
    Q_UNUSED(event);
    Q_UNUSED(idObject);
    Q_UNUSED(idChild);
    Q_UNUSED(dwEventThread);
    Q_UNUSED(dwmsEventTime);

    for(auto it=WindowsUtils::thumbInfoTable.begin(); it!=WindowsUtils::thumbInfoTable.end(); ++it)
    {
        if((HWND)it.value().srcID == hwnd)
        {
            utils->moveThumbnail(it.key(), QSize());
        }
    }
}

int WindowsUtils::createThumbnail(WId srcID, QLabel *dest, bool withFrame, bool noScale)
{
    ThumbInfo ti;
    ti.srcID = srcID;
    ti.dest = dest;
    ti.withFrame = withFrame;
    ti.noScale = noScale;
    ti.scale = -1.0;
    ti.srcSize = QSize();

    BOOL dwmEnabled = false;
    DwmIsCompositionEnabled(&dwmEnabled);
    if(!dwmEnabled)
    {
        dest->setText("Podgląd okna niedostępny\nNależy włączyć Aero Glass");
        return -1;
    }

    HRESULT hr = DwmRegisterThumbnail((HWND)dest->window()->winId(), (HWND)srcID, &ti.thumb);
    if(!SUCCEEDED(hr)) return -1;

    DWM_THUMBNAIL_PROPERTIES props;

    props.dwFlags = DWM_TNP_VISIBLE |
            DWM_TNP_RECTDESTINATION |
            DWM_TNP_SOURCECLIENTAREAONLY;
    QPoint leftTop = dest->mapTo(dest->window(), QPoint(0,0));
    props.rcDestination.left = leftTop.x();
    props.rcDestination.top = leftTop.y();
    props.rcDestination.right = leftTop.x()+dest->width();
    props.rcDestination.bottom = leftTop.y()+dest->height();

    props.fVisible = TRUE;
    props.fSourceClientAreaOnly = !withFrame;
    DwmUpdateThumbnailProperties(ti.thumb, &props);

    DWORD pid;
    DWORD tid = GetWindowThreadProcessId((HWND)srcID, &pid);

    ti.hook = SetWinEventHook(EVENT_OBJECT_LOCATIONCHANGE,
                              EVENT_OBJECT_LOCATIONCHANGE,
                              NULL,
                              WinEventProc,
                              pid,
                              tid,
                              WINEVENT_OUTOFCONTEXT | WINEVENT_SKIPOWNPROCESS);

    int id = currThumbInfoIdx++;
    thumbInfoTable.insert(id, ti);

    this->moveThumbnail(id, QSize());

    return id;
}

void WindowsUtils::setThumbnailFPS(int fps)
{
    Q_UNUSED(fps);
}

QMargins WindowsUtils::windows10IsTerrible(HWND hwnd)
{
    if(!isWin10orGreater)
        return QMargins(0, 0, 0, 0);

    RECT rect, frame;
    GetWindowRect(hwnd, &rect);
    DwmGetWindowAttribute(hwnd, DWMWA_EXTENDED_FRAME_BOUNDS, &frame, sizeof(RECT));

    return QMargins(frame.left-rect.left, frame.top-rect.top, rect.right-frame.right, rect.bottom-frame.bottom);
}

void WindowsUtils::moveThumbnail(int id, QSize srcSize)
{
    if(!thumbInfoTable.contains(id))
        return;

    ThumbInfo *ti = &thumbInfoTable[id];

    if(!ti->thumb) return;

    if(!srcSize.isValid())
    {
        SIZE size;
        if(DwmQueryThumbnailSourceSize(ti->thumb, &size) == S_OK)
            srcSize = QSize(size.cx, size.cy);
    }

    if(srcSize.isValid())
    {
        ti->srcSize = srcSize;
    }
    else
    {
        srcSize = ti->srcSize;
    }

    QSize thumbSize = ti->noScale ? srcSize : srcSize.scaled(ti->dest->size(), Qt::KeepAspectRatio);

    DWM_THUMBNAIL_PROPERTIES props;
    props.dwFlags = 0;

    QMargins offset = windows10IsTerrible((HWND)ti->srcID);

    if(ti->scale > 0) // if scale is set
    {
        props.dwFlags |= DWM_TNP_RECTSOURCE;
        QSize size = ti->dest->size()/ti->scale;

        props.rcSource.top = qMax(0, (srcSize.height()-size.height())/2)+offset.top();
        props.rcSource.left = qMax(0, (srcSize.width()-size.width())/2)+offset.left();
        props.rcSource.bottom = srcSize.height() - props.rcSource.top+offset.top();
        props.rcSource.right = srcSize.width() - props.rcSource.left+offset.left();
        thumbSize = QSize(props.rcSource.right-props.rcSource.left, props.rcSource.bottom-props.rcSource.top)*ti->scale;
    }
    else
    {
        props.dwFlags |= DWM_TNP_RECTSOURCE;
        props.rcSource.top = offset.top();
        props.rcSource.left = offset.left();
        props.rcSource.bottom = srcSize.height()+offset.top();
        props.rcSource.right = srcSize.width()+offset.left();
    }

    props.dwFlags |= DWM_TNP_RECTDESTINATION;
    QPoint leftTop = ti->dest->mapTo(ti->dest->window(), QPoint(0,0));
    props.rcDestination.top = leftTop.y()+(ti->dest->height()-thumbSize.height())/2;
    props.rcDestination.left = leftTop.x()+(ti->dest->width()-thumbSize.width())/2;
    props.rcDestination.right = props.rcDestination.left+thumbSize.width();
    props.rcDestination.bottom = props.rcDestination.top+thumbSize.height();

    DwmUpdateThumbnailProperties(ti->thumb, &props);
}

void WindowsUtils::showThumbnail(int id, bool visible)
{
    if(!thumbInfoTable.contains(id))
        return;

    ThumbInfo *ti = &thumbInfoTable[id];

    if(!ti->thumb) return;

    DWM_THUMBNAIL_PROPERTIES props;
    props.dwFlags = DWM_TNP_VISIBLE;
    props.fVisible = visible;

    DwmUpdateThumbnailProperties(ti->thumb, &props);
}

void WindowsUtils::setThumbnailOpacity(int id, int opacity)
{
    if(!thumbInfoTable.contains(id))
        return;

    ThumbInfo *ti = &thumbInfoTable[id];

    if(!ti->thumb) return;

    DWM_THUMBNAIL_PROPERTIES props;
    props.dwFlags = DWM_TNP_OPACITY;
    props.opacity = opacity;

    DwmUpdateThumbnailProperties(ti->thumb, &props);
}

QPoint WindowsUtils::mapPointToThumbnail(int id, QPoint point)
{
    if(!thumbInfoTable.contains(id))
        return QPoint();

    ThumbInfo *ti = &thumbInfoTable[id];

    if(!ti->thumb) return QPoint();

    if(ti->scale > 0)
    {
        // TODO: unimplemented
    }
    else if(ti->noScale)
    {
        return point - QPoint((ti->srcSize.width()-ti->dest->size().width())/2,
                              (ti->srcSize.height()-ti->dest->size().height())/2);
    }
    else
    {
        float scale = qMin((float)ti->dest->size().width()/ti->srcSize.width(),
                           (float)ti->dest->size().height()/ti->srcSize.height());

        return (point * scale) - QPoint((ti->srcSize.width()*scale-ti->dest->size().width())/2,
                                        (ti->srcSize.height()*scale-ti->dest->size().height())/2);
    }

    return QPoint();
}

QPixmap WindowsUtils::getCursorForThumbnail(int id, QPoint *hotspot, bool forcePixmap)
{
    static QImage oldCursor;
    static QPoint oldHotspot;

    if(!thumbInfoTable.contains(id))
        return QPixmap();

    ThumbInfo *ti = &thumbInfoTable[id];

    if(!ti->thumb) return QPixmap();

    if(cursorChanged)
    {
        CURSORINFO ci;
        ci.cbSize = sizeof(ci);
        GetCursorInfo(&ci);

        ICONINFO ii;
        GetIconInfo(ci.hCursor, &ii);

        QImage bmColor = QtWin::fromHBITMAP(ii.hbmColor, QtWin::HBitmapAlpha).toImage();
        QImage bmMask = QtWin::fromHBITMAP(ii.hbmMask).toImage();

        if(bmColor.isNull()) // black and white cursor
        {
            bmColor = QImage(bmMask.width(), bmMask.height()/2, QImage::Format_RGBA8888);

            for(int y=0; y<bmMask.height()/2; y++)
            {
                QRgb *color = (QRgb*)bmColor.scanLine(y);
                const QRgb *andMask = (QRgb*)bmMask.constScanLine(y);
                const QRgb *xorMask = (QRgb*)bmMask.constScanLine(y+bmMask.height()/2);
                for(int x=0; x<bmColor.width(); x++)
                {
                    ((unsigned char*)&color[x])[0] = (xorMask[x] & 0x1) ? 0xff : 0x00;
                    ((unsigned char*)&color[x])[1] = (xorMask[x] & 0x1) ? 0xff : 0x00;
                    ((unsigned char*)&color[x])[2] = (xorMask[x] & 0x1) ? 0xff : 0x00;
                    ((unsigned char*)&color[x])[3] = ((andMask[x] & 0x1) && !(xorMask[x] & 0x1)) ? 0x00 : 0xff;
                }
            }
        }
        else
        {
            for(int y=0; y<bmColor.height(); y++)
            {
                QRgb *color = (QRgb*)bmColor.scanLine(y);
                const QRgb *mask = (QRgb*)bmMask.constScanLine(y);
                for(int x=0; x<bmColor.width(); x++)
                {
                    ((unsigned char*)&color[x])[3] = ~((unsigned char*)&mask[x])[0];
                }
            }
        }

        DeleteObject(ii.hbmColor);
        DeleteObject(ii.hbmMask);

        oldHotspot.setX(ii.xHotspot);
        oldHotspot.setY(ii.yHotspot);

        oldCursor = bmColor;
    }

    float scale = qMin((float)ti->dest->size().width()/ti->srcSize.width(),
                       (float)ti->dest->size().height()/ti->srcSize.height());

    hotspot->setX(oldHotspot.x()*scale);
    hotspot->setY(oldHotspot.y()*scale);

    if(!ti->noScale)
    {
        static float oldScale;

        if(oldScale != scale)
        {
            cursorChanged = true;
            oldScale = scale;
        }

        if(cursorChanged || forcePixmap)
        {
            cursorChanged = false;
            return QPixmap::fromImage(oldCursor.scaled(oldCursor.size() * scale, Qt::KeepAspectRatio, Qt::SmoothTransformation));
        }
    }
    else if(cursorChanged || forcePixmap)
    {
        cursorChanged = false;
        return QPixmap::fromImage(oldCursor);
    }

    cursorChanged = false;
    return QPixmap();
}

void WindowsUtils::setThumbnailScale(int id, float scale)
{
    if(!thumbInfoTable.contains(id))
        return;

    ThumbInfo *ti = &thumbInfoTable[id];

    if(!ti->thumb) return;

    ti->scale = scale;
    this->moveThumbnail(id, QSize());
}

void WindowsUtils::destroyThumbnail(int id)
{
    if(!thumbInfoTable.contains(id))
        return;

    DwmUnregisterThumbnail(thumbInfoTable[id].thumb);

    UnhookWinEvent(thumbInfoTable[id].hook);

    thumbInfoTable.remove(id);
}

void WindowsUtils::initPeakMeter(qint64 pid)
{
    //TODO: Release interfaces

    HRESULT hr = S_OK;
    IMMDeviceEnumerator *pMMDeviceEnumerator = NULL;
    IMMDeviceCollection *pMMDeviceCollection = NULL;

    hr = CoCreateInstance(
                __uuidof(MMDeviceEnumerator), NULL,
                CLSCTX_ALL, __uuidof(IMMDeviceEnumerator),
                (void**)&pMMDeviceEnumerator);
    if (FAILED(hr))
    {
        return;
    }

    hr = pMMDeviceEnumerator->EnumAudioEndpoints(eRender, DEVICE_STATE_ACTIVE, &pMMDeviceCollection);
    if (FAILED(hr))
    {
        LOG(L"IMMDeviceEnumerator::EnumAudioEndpoints failed: hr = 0x%08x", hr);
        return;
    }

    UINT32 nDevices;
    hr = pMMDeviceCollection->GetCount(&nDevices);
    if (FAILED(hr))
    {
        LOG(L"IMMDeviceCollection::GetCount failed: hr = 0x%08x", hr);
        return;
    }

    for (UINT32 d = 0; d < nDevices; d++)
    {
        IMMDevice *pMMDevice = NULL;
        hr = pMMDeviceCollection->Item(d, &pMMDevice);
        if (FAILED(hr))
        {
            LOG(L"IMMDeviceCollection::Item failed: hr = 0x%08x", hr);
            continue;
        }

        // get a session enumerator
        IAudioSessionManager2 *pAudioSessionManager2 = NULL;
        hr = pMMDevice->Activate(
                    __uuidof(IAudioSessionManager2),
                    CLSCTX_ALL,
                    nullptr,
                    reinterpret_cast<void **>(&pAudioSessionManager2));
        if (FAILED(hr))
        {
            LOG(L"IMMDevice::Activate(IAudioSessionManager2) failed: hr = 0x%08x", hr);
            return;
        }

        IAudioSessionEnumerator *pAudioSessionEnumerator = NULL;
        hr = pAudioSessionManager2->GetSessionEnumerator(&pAudioSessionEnumerator);
        if (FAILED(hr))
        {
            LOG(L"IAudioSessionManager2::GetSessionEnumerator() failed: hr = 0x%08x", hr);
            return;
        }

        // iterate over all the sessions
        int count = 0;
        hr = pAudioSessionEnumerator->GetCount(&count);
        if (FAILED(hr))
        {
            LOG(L"IAudioSessionEnumerator::GetCount() failed: hr = 0x%08x", hr);
            return;
        }

        for (int session = 0; session < count; session++)
        {
            // get the session identifier
            IAudioSessionControl *pAudioSessionControl;
            hr = pAudioSessionEnumerator->GetSession(session, &pAudioSessionControl);
            if (FAILED(hr))
            {
                LOG(L"IAudioSessionEnumerator::GetSession() failed: hr = 0x%08x", hr);
                return;
            }

            IAudioSessionControl2 *pAudioSessionControl2;
            hr = pAudioSessionControl->QueryInterface(IID_PPV_ARGS(&pAudioSessionControl2));
            if (FAILED(hr))
            {
                LOG(L"IAudioSessionControl::QueryInterface(IAudioSessionControl2) failed: hr = 0x%08x", hr);
                return;
            }

            DWORD processId = 0;
            hr = pAudioSessionControl2->GetProcessId(&processId);
            if (FAILED(hr))
            {
                LOG(L"IAudioSessionControl2::GetProcessId() failed: hr = 0x%08x", hr);
                return;
            }

            if(processId != pid) continue;

            // get the current audio peak meter level for this session
            hr = pAudioSessionControl->QueryInterface(IID_PPV_ARGS(&pAudioMeterInformation));
            if (FAILED(hr))
            {
                LOG(L"IAudioSessionControl::QueryInterface(IAudioMeterInformation) failed: hr = 0x%08x", hr);
                return;
            }
        }
    }

    this->peakTimer.start(100);

}

void WindowsUtils::uninitPeakMeter()
{
    CoUninitialize();
}

void WindowsUtils::getPeak()
{
    float peak = 0.0f;
    if(pAudioMeterInformation)
    {
        HRESULT hr = pAudioMeterInformation->GetPeakValue(&peak);
        if (FAILED(hr)) {
            LOG(L"IAudioMeterInformation::GetPeakValue() failed: hr = 0x%08x", hr);
        }
    }

    emit peakLevelChanged(peak);
}

WindowInfo WindowsUtils::getWindowAt(QPoint pos, WId skipWindow)
{
    WindowInfo wi;

    HWND hwnd = GetTopWindow(NULL);
    do
    {
        if(hwnd == (HWND)skipWindow)
            continue;

        WINDOWINFO winInfo;
        GetWindowInfo(hwnd, &winInfo);

        if(!(winInfo.dwStyle & WS_VISIBLE))
            continue;

        RECT *rect = &winInfo.rcWindow;
        wi.geometry = QRect(QPoint(rect->left, rect->top), QPoint(rect->right, rect->bottom));

        if(wi.geometry.contains(pos, true))
        {
            wi.geometry -= windows10IsTerrible(hwnd);
            wi.geometry -= QMargins(0, 0, 1, 1);
            wi.windowID = (WId)hwnd;
            return wi;
        }
    } while(hwnd = GetNextWindow(hwnd, GW_HWNDNEXT));

    wi.windowID = 0;
    wi.geometry = QRect(0,0,0,0);
    return wi;
}

QString WindowsUtils::getWindowTitle(WId winID)
{
    WCHAR text[256];
    GetWindowTextW((HWND)winID, text, 256);
    if(GetLastError() == ERROR_SUCCESS)
    {
        return QString::fromUtf16((const ushort*)text);
    }
    else
    {
        return QString();
    }
}

QRect WindowsUtils::getWindowRect(WId winID)
{
    RECT rect;
    bool ok = GetWindowRect((HWND)winID, &rect);

    if(ok && !IsIconic((HWND)winID))
        return QRect(rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top)
                .marginsRemoved(windows10IsTerrible((HWND)winID));
    else
        return QRect();
}

void WindowsUtils::setWindowSize(WId winID, QSize size)
{
    RECT rcWind;
    GetWindowRect((HWND)winID, &rcWind);
    QMargins border = windows10IsTerrible((HWND)winID);
    MoveWindow((HWND)winID, rcWind.left, rcWind.top, size.width()+border.left()+border.right(), size.height()+border.top()+border.bottom(), TRUE);
}

LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam)
{
    if(wParam == WM_MOUSEMOVE)
    {
        MSLLHOOKSTRUCT *info = (MSLLHOOKSTRUCT*)lParam;
        emit utils->mouseMoved(QPoint(info->pt.x, info->pt.y));
    }

    return CallNextHookEx(mouseMoveHook, nCode, wParam, lParam);
}

void CALLBACK WinEventMouseProc(HWINEVENTHOOK hWinEventHook, DWORD event, HWND hwnd, LONG idObject, LONG idChild, DWORD dwEventThread, DWORD dwmsEventTime)
{
    Q_UNUSED(hWinEventHook);
    Q_UNUSED(event);
    Q_UNUSED(dwEventThread);
    Q_UNUSED(dwmsEventTime);

    if (hwnd == nullptr && idObject == OBJID_CURSOR && idChild == CHILDID_SELF)
    {
        cursorChanged = true;
    }
}

void WindowsUtils::watchMouseMove(bool watch)
{
    if(watch)
    {
        mouseMoveHook = SetWindowsHookExW(WH_MOUSE_LL, MouseProc, NULL, 0);

        cursorChanged = true;
        mouseChangeHook = SetWinEventHook(EVENT_OBJECT_NAMECHANGE,
                                          EVENT_OBJECT_NAMECHANGE,
                                          NULL,
                                          WinEventMouseProc,
                                          0,
                                          0,
                                          WINEVENT_OUTOFCONTEXT);
    }
    else
    {
        UnhookWindowsHookEx(mouseMoveHook);
        UnhookWinEvent(mouseChangeHook);
    }
}

QString WindowsUtils::getDataDir()
{
    return QApplication::applicationDirPath() + "/";
}
