/*
    ModernNewel2Video
    Copyright (C) 2016  Lukasz Matczak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "linuxutils.h"

#include <QDebug>
#include <QScreen>
#include <QGuiApplication>
#include <QApplication>
#include <QX11Info>
#include <QGraphicsOpacityEffect>

#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/extensions/Xcomposite.h>
#include <X11/extensions/Xfixes.h>

int handler(Display *d, XErrorEvent *e)
{
    char buf[100];
    XGetErrorText(d, e->error_code, buf, 99);
    qDebug() << "ERROR: " << buf;

    return 0;
}

LinuxUtils::LinuxUtils()
{
    XSetErrorHandler(&handler);

    watchMouse = false;

    dpy = QX11Info::display();
    rootWindow = RootWindow(dpy, QX11Info::appScreen());

    connect(&updateTimer, &QTimer::timeout, this, &LinuxUtils::update);
    updateTimer.start(30);
}

LinuxUtils::~LinuxUtils()
{

}

QString LinuxUtils::getFriendlyName(QString dev)
{
    return dev;
}

void LinuxUtils::setCursorClipGeom(QRect screen)
{
    Q_UNUSED(screen);
}

void LinuxUtils::enableCursorClip(bool enabled)
{
    Q_UNUSED(enabled);
}

void LinuxUtils::setExtendScreenTopology()
{

}

void LinuxUtils::setPreviousScreenTopology()
{

}

int LinuxUtils::createThumbnail(WId srcID, QLabel *dest, bool withFrame, bool noScale)
{
    Q_UNUSED(withFrame);

    ThumbInfo ti;
    ti.srcID = srcID;
    ti.dest = dest;
    ti.noScale = noScale;
    ti.scale = -1.0;
    ti.srcSize = QSize();
    ti.oldXImage = nullptr;

    int event_base, error_base;
    if (XCompositeQueryExtension(dpy, &event_base, &error_base))
    {
        int major = 0, minor = 2;
        XCompositeQueryVersion(dpy, &major, &minor);

        if(!(major > 0 || minor >= 2))
        {
            dest->setText("Podgląd okna niedostępny\nWymagane jest rozszerzenie XComposite\nw wersji 0.2 lub nowszej");
            return -1;
        }
    }

    XCompositeRedirectWindow(dpy, srcID, CompositeRedirectAutomatic);

    int id = currThumbInfoIdx++;
    thumbInfoTable.insert(id, ti);
    return id;
}

void LinuxUtils::setThumbnailFPS(int fps)
{
    this->thumbnailFPS = fps;
    this->updateTimer.setInterval(1000/fps);
}

void LinuxUtils::update()
{
    static QPoint oldPos;

    if(watchMouse)
    {
        Window root_return, child_return;
        int root_x_return, root_y_return;
        int win_x_return, win_y_return;
        unsigned int mask_return;
        XQueryPointer(dpy, rootWindow, &root_return, &child_return, &root_x_return, &root_y_return, &win_x_return, &win_y_return, &mask_return);

        QPoint pos = QPoint(root_x_return, root_y_return);

        if(pos != oldPos)
        {
            oldPos = pos;
            emit mouseMoved(pos);
        }
    }

    QList<int> keys = this->thumbInfoTable.keys();
    foreach(int id, keys)
    {
        this->updateThumbnail(id);
    }
}

void LinuxUtils::updateThumbnail(int id)
{
    if(!thumbInfoTable.contains(id))
        return;

    ThumbInfo *ti = &thumbInfoTable[id];

    Pixmap pix = XCompositeNameWindowPixmap(dpy, ti->srcID);

    ti->srcSize = this->getWindowRect(ti->srcID).size();

    XImage *ximage;
    if(ti->noScale)
        ximage = XGetImage(dpy,
                           pix,
                           qMax(0, (ti->srcSize.width()-ti->dest->width())/2),
                           qMax(0, (ti->srcSize.height()-ti->dest->height())/2),
                           qMin(ti->srcSize.width(), ti->dest->width()),
                           qMin(ti->srcSize.height(), ti->dest->height()),
                           AllPlanes,
                           ZPixmap);
    else
        ximage = XGetImage(dpy, pix, 0, 0, ti->srcSize.width(), ti->srcSize.height(), AllPlanes, ZPixmap);

    if(ximage)
    {
        QImage qimg((uchar*)ximage->data, ximage->width, ximage->height, ximage->bytes_per_line, QImage::Format_RGB32 /*TODO*/);

        if(ti->scale > 0) // if scale is set
            qimg = qimg.scaled(ti->srcSize*ti->scale, Qt::KeepAspectRatio, Qt::SmoothTransformation);
        else if(!ti->noScale)
            qimg = qimg.scaled(ti->dest->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);

        ti->dest->setPixmap(QPixmap::fromImage(qimg));

        if(ti->oldXImage)
            XDestroyImage(ti->oldXImage);

        ti->oldXImage = ximage;
    }

    XFreePixmap(dpy, pix);
}

void LinuxUtils::moveThumbnail(int id, QSize srcSize)
{
    Q_UNUSED(id);
    Q_UNUSED(srcSize);
}

void LinuxUtils::showThumbnail(int id, bool visible)
{
    if(!thumbInfoTable.contains(id))
        return;

    ThumbInfo *ti = &thumbInfoTable[id];

    ti->dest->setVisible(visible);
}

void LinuxUtils::setThumbnailOpacity(int id, int opacity)
{
    if(!thumbInfoTable.contains(id))
        return;

    ThumbInfo *ti = &thumbInfoTable[id];

    QWidget *widget = ti->dest;
    QGraphicsOpacityEffect *effect = new QGraphicsOpacityEffect(widget);
    effect->setOpacity(opacity/255.0);
    widget->setGraphicsEffect(effect);
}

void LinuxUtils::setThumbnailScale(int id, float scale)
{
    ThumbInfo *ti = &thumbInfoTable[id];

    if(!ti->srcID) return;

    ti->scale = scale;
}

QPoint LinuxUtils::mapPointToThumbnail(int id, QPoint point)
{
    if(!thumbInfoTable.contains(id))
        return QPoint();

    ThumbInfo *ti = &thumbInfoTable[id];

    if(!ti->srcID) return QPoint();

    if(ti->scale > 0)
    {
        // TODO: unimplemented
    }
    else if(ti->noScale)
    {
        return point - QPoint((ti->srcSize.width()-ti->dest->size().width())/2,
                              (ti->srcSize.height()-ti->dest->size().height())/2);
    }
    else
    {
        float scale = qMin((float)ti->dest->size().width()/ti->srcSize.width(),
                           (float)ti->dest->size().height()/ti->srcSize.height());

        return (point * scale) - QPoint((ti->srcSize.width()*scale-ti->dest->size().width())/2,
                                        (ti->srcSize.height()*scale-ti->dest->size().height())/2);
    }

    return QPoint();
}

QPixmap LinuxUtils::getCursorForThumbnail(int id, QPoint *hotspot, bool forcePixmap)
{
    static QImage oldCursor;
    static unsigned long oldSerial;

    if(!thumbInfoTable.contains(id))
        return QPixmap();

    ThumbInfo *ti = &thumbInfoTable[id];

    if(!ti->srcID) return QPixmap();

    XFixesCursorImage *cursor = XFixesGetCursorImage(dpy);
    if(cursor == nullptr) return QPixmap();

    float scale = qMin((float)ti->dest->size().width()/ti->srcSize.width(),
                       (float)ti->dest->size().height()/ti->srcSize.height());

    hotspot->setX(cursor->xhot*scale);
    hotspot->setY(cursor->yhot*scale);

    bool cursorChanged = (cursor->cursor_serial != oldSerial);

    if(cursorChanged)
    {
        QImage img = QImage(cursor->width, cursor->height, QImage::Format_ARGB32_Premultiplied);

        for(int y=0; y<img.height(); y++)
        {
            QRgb *color = (QRgb*)img.scanLine(y);
            for(int x=0; x<img.width(); x++)
            {
                color[x] = cursor->pixels[cursor->width*y+x];
            }
        }

        oldSerial = cursor->cursor_serial;
        oldCursor = img;
    }

    if(!ti->noScale)
    {
        static float oldScale;

        if(oldScale != scale)
        {
            cursorChanged = true;
            oldScale = scale;
        }

        if(cursorChanged || forcePixmap)
        {
            return QPixmap::fromImage(oldCursor.scaled(oldCursor.size()*scale, Qt::KeepAspectRatio, Qt::SmoothTransformation));
        }
    }
    else if(cursorChanged || forcePixmap)
    {
        return QPixmap::fromImage(oldCursor);
    }

    return QPixmap();
}

void LinuxUtils::destroyThumbnail(int id)
{
    if(!thumbInfoTable.contains(id))
        return;

    ThumbInfo *ti = &thumbInfoTable[id];

    XCompositeUnredirectWindow(dpy, ti->srcID, CompositeRedirectAutomatic);

    if(ti->oldXImage)
        XDestroyImage(ti->oldXImage);

    thumbInfoTable.remove(id);
}

void LinuxUtils::initPeakMeter(qint64 pid)
{
    Q_UNUSED(pid);
}

void LinuxUtils::uninitPeakMeter()
{

}

WindowInfo LinuxUtils::getWindowAt(QPoint pos, WId skipWindow)
{
    WindowInfo wi;

    Atom prop = XInternAtom(dpy, "_NET_CLIENT_LIST_STACKING", False), type;
    int form;
    unsigned long remain, length;
    Window *windows;
    if(XGetWindowProperty(dpy, rootWindow, prop, 0, 1024, False, XA_WINDOW, &type, &form, &length, &remain, (unsigned char**)&windows) == Success)
    {
        for(int i=length-1; i>=0; i--)
        {
            if(windows[i] == skipWindow)
                continue;

            XWindowAttributes winAttr;
            XGetWindowAttributes(dpy, windows[i], &winAttr);

            if(winAttr.map_state != IsViewable)
                continue;

            wi.geometry = this->getWindowRect(windows[i]);
            if(wi.geometry.contains(pos, true))
            {
                wi.windowID = (WId)windows[i];
                XFree(windows);
                return wi;
            }
        }
        XFree(windows);
    }

    wi.windowID = 0;
    wi.geometry = QRect(0, 0, 0, 0);
    return wi;
}

QString LinuxUtils::getWindowTitle(WId winID)
{
    Atom prop = XInternAtom(dpy, "_NET_WM_NAME", False), type;
    int form;
    unsigned long remain, len;
    unsigned char *title;

    if(XGetWindowProperty(dpy, winID, prop, 0, 1024, False, AnyPropertyType, &type, &form, &len, &remain, &title) != Success)
        qDebug() << "XGetWindowProperty error";

    QString str = QString::fromLocal8Bit((char*)title);
    XFree(title);
    return str;
}

QRect LinuxUtils::getWindowRect(WId winID)
{
    Window junkroot;
    int x, y, junkx, junky;
    unsigned int width, height, border, depth;

    if(XGetGeometry(dpy, winID, &junkroot, &junkx, &junky, &width, &height, &border, &depth) != Success)
        if(XTranslateCoordinates(dpy, winID, junkroot, 0, 0, &x, &y, &junkroot) != Success)
            return QRect(x, y, width+2*border, height+2*border);

    return QRect();
}

void LinuxUtils::setWindowSize(WId winID, QSize size)
{
    XResizeWindow(dpy, winID, size.width(), size.height());
}

void LinuxUtils::watchMouseMove(bool watch)
{
    watchMouse = watch;
}

QString LinuxUtils::getDataDir()
{
    return "/usr/share/ModernNewel2Video/";
}
