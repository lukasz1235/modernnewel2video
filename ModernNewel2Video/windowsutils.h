/*
    ModernNewel2Video
    Copyright (C) 2015-2016  Lukasz Matczak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WINDOWSUTILS_H
#define WINDOWSUTILS_H

#include "osutils.h"

#include <QTimer>

#include <Windows.h>
#include <dwmapi.h>
#include <Endpointvolume.h>

class WindowsUtils : public OSUtils
{
    Q_OBJECT
public:
    WindowsUtils();
    ~WindowsUtils();

    QString getFriendlyName(QString dev);
    QString getOutputTechnologyString(int number);

    void setCursorClipGeom(QRect screen);
    void enableCursorClip(bool enabled);

    void setExtendScreenTopology();
    void setPreviousScreenTopology();

    int createThumbnail(WId srcID, QLabel *dest, bool withFrame, bool noScale);
    void setThumbnailFPS(int fps);
    void showThumbnail(int id, bool visible);
    void moveThumbnail(int id, QSize srcSize);
    void setThumbnailOpacity(int id, int opacity);
    void setThumbnailScale(int id, float scale);
    QPoint mapPointToThumbnail(int id, QPoint point);
    QPixmap getCursorForThumbnail(int id, QPoint *hotspot, bool forcePixmap);
    void destroyThumbnail(int id);

    void initPeakMeter(qint64 pid);
    void uninitPeakMeter();

    WindowInfo getWindowAt(QPoint pos, WId skipWindow);
    QString getWindowTitle(WId winID);
    QRect getWindowRect(WId winID);
    void setWindowSize(WId winID, QSize size);

    void watchMouseMove(bool watch);

    QString getDataDir();

    friend void CALLBACK WinEventProc(HWINEVENTHOOK hWinEventHook, DWORD event, HWND hwnd, LONG idObject, LONG idChild, DWORD dwEventThread, DWORD dwmsEventTime);

    struct ThumbInfo
    {
        WId srcID;
        QLabel *dest;
        HTHUMBNAIL thumb;
        HWINEVENTHOOK hook;
        bool withFrame;
        bool noScale;
        QSize srcSize;
        float scale;
    };

private:
    QMargins windows10IsTerrible(HWND hwnd);

    static QMap<int, ThumbInfo> thumbInfoTable;
    static int currThumbInfoIdx;

    RECT area;

    DISPLAYCONFIG_TOPOLOGY_ID topologyId;

    IAudioMeterInformation *pAudioMeterInformation;
    QTimer peakTimer;

    bool isWin10orGreater;

private slots:
    void getPeak();

};

#endif // WINDOWSUTILS_H
