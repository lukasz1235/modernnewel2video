#-------------------------------------------------
#
# Project created by QtCreator 2015-11-01T10:42:23
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

win32: QT += network winextras
unix: QT += x11extras

TARGET = ModernNewel2Video
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)

SOURCES += main.cpp\
        mainwindow.cpp \
    settings.cpp \
    playlistmodel.cpp \
    videowindow.cpp \
    flickcharm.cpp \
    mpv.cpp \
    windowselector.cpp

win32: SOURCES += windowsutils.cpp updatedialog.cpp
unix: SOURCES += linuxutils.cpp

HEADERS  += mainwindow.h \
    sliderstyle.h \
    settings.h \
    playlistmodel.h \
    videowindow.h \
    flickcharm.h \
    osutils.h \
    mpv.h \
    windowselector.h

win32: HEADERS += windowsutils.h updatedialog.h
unix: HEADERS += linuxutils.h

FORMS    += mainwindow.ui \
    settings.ui \

win32: FORMS +=  updatedialog.ui

RC_FILE = mn2v.rc

win32: LIBS += -L$$PWD/ -luser32 -lole32 -ldwmapi -lgdi32

RESOURCES += \
    img.qrc

win32: RESOURCES += windows.qrc

LIBS += -ltag

unix: LIBS += -lX11 -lXcomposite -lXfixes

win32: INCLUDEPATH += $$PWD/../../../updater/updater $$PWD/../../../taglib/include
win32: LIBS += -lupdater -L$$PWD/../../../updater/build-updater-Desktop_Qt_5_7_0_MSVC2015_32bit-Release/release/ -L$$PWD/../../../taglib/lib

CONFIG(debug, debug|release) {
MY_DESTDIR_TARGET = "$$OUT_PWD/debug"
}
CONFIG(release, debug|release) {
MY_DESTDIR_TARGET = "$$OUT_PWD/release"
}

win32:CONFIG(release, debug|release) {
WINSDK_DIR = C:/Program Files (x86)/Microsoft SDKs/Windows/v7.0A
WIN_PWD = $$replace(PWD, /, \\)
DESTDIR_TARGET_WIN = $$replace(MY_DESTDIR_TARGET, /, \\)
QMAKE_POST_LINK += "$$WINSDK_DIR/bin/mt.exe -manifest $$quote($$WIN_PWD\\$$basename(TARGET).exe.manifest) -outputresource:$$quote($$DESTDIR_TARGET_WIN\\$$basename(TARGET).exe;1)"
}

win32: LIBS += -L$$PWD/../../taglib/lib/

target.path = /usr/bin
INSTALLS += target

script.path = /usr/share/ModernNewel2Video
script.files = script.lua

INSTALLS += script
