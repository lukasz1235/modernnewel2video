/*
    ModernNewel2Video
    Copyright (C) 2015-2016  Lukasz Matczak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OSUTILS_H
#define OSUTILS_H

#include "videowindow.h"

#include <QRect>
#include <QLabel>

struct WindowInfo
{
    WId windowID;
    QRect geometry;
};

class OSUtils : public QObject
{
    Q_OBJECT
public:
    virtual QString getFriendlyName(QString dev) = 0;

    virtual void setCursorClipGeom(QRect screen) = 0;
    virtual void enableCursorClip(bool enabled) = 0;

    virtual void setExtendScreenTopology() = 0;
    virtual void setPreviousScreenTopology() = 0;

    virtual int createThumbnail(WId srcID, QLabel *dest, bool withFrame, bool noScale) = 0;
    virtual void setThumbnailFPS(int fps) = 0;
    virtual void showThumbnail(int id, bool visible) = 0;
    virtual void moveThumbnail(int id, QSize srcSize) = 0;
    virtual void setThumbnailOpacity(int id, int opacity) = 0;
    virtual void setThumbnailScale(int id, float scale) = 0;
    virtual QPoint mapPointToThumbnail(int id, QPoint point) = 0;
    virtual QPixmap getCursorForThumbnail(int id, QPoint *hotspot, bool forcePixmap) = 0;
    virtual void destroyThumbnail(int id) = 0;

    virtual void initPeakMeter(qint64 pid) = 0;
    virtual void uninitPeakMeter() = 0;

    virtual WindowInfo getWindowAt(QPoint pos, WId skipWindow) = 0;
    virtual QString getWindowTitle(WId winID) = 0;
    virtual QRect getWindowRect(WId winID) = 0;
    virtual void setWindowSize(WId winID, QSize size) = 0;

    virtual void watchMouseMove(bool watch) = 0;

    virtual QString getDataDir() = 0;

signals:
    void peakLevelChanged(double peakLevel);
    void mouseMoved(QPoint pos);
};

#endif // OSUTILS_H
