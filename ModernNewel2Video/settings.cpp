/*
    ModernNewel2Video
    Copyright (C) 2015-2016  Lukasz Matczak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "settings.h"
#include "ui_settings.h"
#include "mainwindow.h"

#include <QPushButton>
#include <QDebug>
#include <QFileDialog>
#include <QProxyStyle>
#include <QScreen>
#include <QMessageBox>

#ifdef Q_OS_WIN
    #include <Shobjidl.h>
#endif

QVector<AudioInfo> Settings::ainfo;

Settings::Settings(QSettings *set, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings),
    settings(set)
{
    ui->setupUi(this);

    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Anuluj");
    ui->buttonBox->button(QDialogButtonBox::Apply)->setText("Zastosuj");
#ifdef Q_OS_WIN
    QSettings globalSettings("HKEY_LOCAL_MACHINE\\Software\\Lukasz Matczak\\ModernNewel2Video", QSettings::NativeFormat);
    ui->verLbl->setText(QString("ModernNewel2Video [Wersja: %1 (build %2)]").arg(globalSettings.value("currVersion").toString(), globalSettings.value("currBuild").toString()));
#else
    ui->verLbl->setText("ModernNewel2Video");
#endif
    connect(qApp, &QApplication::screenAdded, this, [this](QScreen*){drawMonitors();});
    connect(qApp, &QApplication::screenRemoved, this, [this](QScreen*){drawMonitors();});
    selectedMonitor = settings->value("displayGeometry").toRect();

    setScreenLbl();
    drawMonitors();

    //AudioInfo
    AudioInfo savedAinfo(settings);

    int foundAt = -1;
    for(int i=0; i<ainfo.size(); i++)
    {
        if(ainfo[i] == savedAinfo) foundAt = i;
    }

    if(foundAt == -1 && !savedAinfo.id.isEmpty())
    {
        ainfo.push_front(savedAinfo);
        foundAt = 0;
    }

    foreach(AudioInfo info, ainfo)
    {
        ui->audioCombo->addItem(info.toString(), QVariant::fromValue(info));
    }

    ui->audioCombo->setCurrentIndex(foundAt);

    ui->hideScreenChk->setChecked(settings->value("hideScreen", true).toBool());
    ui->calibrationChk->setChecked(settings->value("calibration", false).toBool());
    ui->hwdecChk->setChecked(settings->value("hwdec", false).toBool());

    ui->autoPlaylistChk->setChecked(settings->value("autoPlaylist", false).toBool());
    ui->oldChk->setChecked(settings->value("deleteOldVideos", false).toBool());
    ui->videoDirEdit->setText(settings->value("videoDir", "").toString());

    on_autoPlaylistChk_stateChanged(0);
    on_hideScreenChk_stateChanged(0);

    ui->videoDirBtn->setIcon(QApplication::style()->standardIcon(QStyle::SP_DirIcon));

#ifdef Q_OS_LINUX
    ui->setAssocBtn->setVisible(false);

    fpsSpin = new QSpinBox(this);
    fpsSpin->setRange(1, 120);
    fpsSpin->setValue(settings->value("previewFPS", 30).toInt());

    ui->formLayout->insertRow(5, "Częstotliwość odświeżania podglądu (FPS): ", fpsSpin);

    ui->label->setText(ui->label->text()+".");
#endif
#ifdef Q_OS_WIN
    ui->label->setText(ui->label->text()+"; <a href=\"LICENSE.txt\">click here</a> for details.");
#endif
}

Settings::~Settings()
{
    delete ui;
}

void Settings::setScreenLbl()
{
    foreach (QScreen *screen, QGuiApplication::screens())
    {
        if(screen->geometry() == selectedMonitor)
        {
            QString friendlyName = utils->getFriendlyName(screen->name());
            friendlyName += QString(" [%1x%2]").arg(screen->size().width()).arg(screen->size().height());
            ui->selectedScreenLbl->setText(friendlyName);
        }
    }
}

void Settings::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    drawMonitors();
}

void Settings::drawMonitors()
{
    QList<QScreen*> screens = QGuiApplication::screens();

    double scale = qMin((ui->monitorsFrame->height()-10.0)/screens[0]->virtualSize().height(),
            (ui->monitorsFrame->width()-10.0)/screens[0]->virtualSize().width());

    while(QPushButton *w = ui->monitorsFrame->findChild<QPushButton*>())
        delete w;

    int minX = screens[0]->virtualGeometry().x();
    int minY = screens[0]->virtualGeometry().y();

    foreach(QScreen *screen, screens)
    {
        QString name = QString("%1x%2").arg(screen->size().width()).arg(screen->size().height());
        QPushButton *monitorBtn = new QPushButton(ui->monitorsFrame);

        if(screen->geometry() == QGuiApplication::primaryScreen()->geometry())
        {
            name += "\nGłówny";
            monitorBtn->setStyleSheet("background-color: rgb(76,76,76);");
        }
        else if(screen->geometry() == selectedMonitor)
        {
            monitorBtn->setStyleSheet("background-color: rgb(0,62,198);");
            monitorBtn->setCursor(QCursor(Qt::PointingHandCursor));
        }
        else
        {
            monitorBtn->setStyleSheet("background-color: rgb(36,36,76);");
            monitorBtn->setCursor(QCursor(Qt::PointingHandCursor));
        }

        monitorBtn->setText(name);

        QRect geom = screen->geometry();
        geom.translate(-minX, -minY);
        monitorBtn->setGeometry(QRect(geom.topLeft()*scale+QPoint(5,5), geom.size()*scale));

        connect(monitorBtn, &QPushButton::clicked, this, [this, screen](){
            if(screen->geometry() == QGuiApplication::primaryScreen()->geometry())
                QMessageBox::warning(this, "", "Nie możesz wybrać głównego ekranu!");
            else
            {
                selectedMonitor = screen->geometry();
                drawMonitors();
            }
        });

        monitorBtn->show();
    }

    setScreenLbl();
}

void Settings::on_buttonBox_clicked(QAbstractButton *button)
{
    if(button == ui->buttonBox->button(QDialogButtonBox::Ok) ||
       button == ui->buttonBox->button(QDialogButtonBox::Apply))
    {
        AudioInfo ainfo = ui->audioCombo->itemData(ui->audioCombo->currentIndex()).value<AudioInfo>();
        ainfo.save(settings);

        settings->setValue("displayGeometry", selectedMonitor);

        settings->setValue("hideScreen", ui->hideScreenChk->isChecked());
        settings->setValue("calibration", ui->calibrationChk->isChecked());
        settings->setValue("hwdec", ui->hwdecChk->isChecked());

        settings->setValue("autoPlaylist", ui->autoPlaylistChk->isChecked());
        settings->setValue("deleteOldVideos", ui->oldChk->isChecked());
        settings->setValue("videoDir", ui->videoDirEdit->text());
#ifdef Q_OS_LINUX
        settings->setValue("previewFPS", this->fpsSpin->value());
#endif
    }
}

void Settings::on_setAssocBtn_clicked()
{
#ifdef Q_OS_WIN
    IApplicationAssociationRegistrationUI *applicationAssociationRegistrationUI = 0;

    CoCreateInstance(CLSID_ApplicationAssociationRegistrationUI,
                     0,
                     CLSCTX_INPROC_SERVER,
                     IID_IApplicationAssociationRegistrationUI,
                     (LPVOID*)&applicationAssociationRegistrationUI);

    if (applicationAssociationRegistrationUI)
         applicationAssociationRegistrationUI->LaunchAdvancedAssociationUI(L"ModernNewel2Video");
#endif
}

void Settings::on_autoPlaylistChk_stateChanged(int arg1)
{
    Q_UNUSED(arg1);
    bool autoPlaylist = ui->autoPlaylistChk->isChecked();
    ui->videoDirLbl->setEnabled(autoPlaylist);
    ui->videoDirEdit->setEnabled(autoPlaylist);
    ui->videoDirBtn->setEnabled(autoPlaylist);
    ui->oldChk->setEnabled(autoPlaylist);
}

void Settings::on_videoDirBtn_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, QString(), ui->videoDirEdit->text());
    if(!path.isEmpty())
    {
        ui->videoDirEdit->setText(QDir::toNativeSeparators(path));
    }
}

void Settings::on_hideScreenChk_stateChanged(int arg1)
{
    Q_UNUSED(arg1);
    ui->calibrationChk->setEnabled(ui->hideScreenChk->isChecked());
}
