/*
    ModernNewel2Video
    Copyright (C) 2015-2016  Lukasz Matczak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "videowindow.h"
#include "settings.h"
#include "windowselector.h"

#include <QDir>
#include <QSettings>
#include <QMessageBox>

#include <QGraphicsDropShadowEffect>
#include <QFileDialog>
#include <QScreen>
#include <QGestureEvent>
#include <QMimeData>
#include <QFontDatabase>
#include <QImageReader>

#ifdef Q_OS_WIN
    #include "windowsutils.h"
    #include "updater.h"
    #include "updatedialog.h"
    #include <QWinTaskbarButton>
    #include <QWinTaskbarProgress>
#endif
#ifdef Q_OS_LINUX
    #include "linuxutils.h"
#endif

OSUtils *utils;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
#ifdef Q_OS_WIN
    utils = new WindowsUtils;
    QDir(QDir::tempPath() + "/ModernNewel2Video").removeRecursively();
#endif
#ifdef Q_OS_LINUX
    utils = new LinuxUtils;

    this->setWindowIcon(QIcon("/usr/share/icons/hicolor/128x128/apps/ModernNewel2Video.png"));
#endif

    QFontDatabase::addApplicationFont(utils->getDataDir() + "/OpenSans-Light.ttf");

    ui->setupUi(this);

    this->canMove = false;
    this->paused = true;
    this->playOnce = true;

#ifdef Q_OS_WIN
    taskbarButton.progress()->setRange(0, 100);
#endif
    this->setStyleSheet("background: transparent;");
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setAttribute(Qt::WA_AcceptTouchEvents);
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowMinimizeButtonHint);
#ifdef Q_OS_WIN
    globalSettings = new QSettings("HKEY_LOCAL_MACHINE\\Software\\Lukasz Matczak\\ModernNewel2Video", QSettings::NativeFormat);
#endif
    settings = new QSettings("Lukasz Matczak", "ModernNewel2Video", this);
    this->restoreGeometry(settings->value("windowGeometry").toByteArray());
    this->resize(577, 633);

    settings->remove("displayIsPrimary");
    settings->remove("monitorName");
    settings->remove("outputTechnology");
    settings->remove("oldVideos");
    settings->remove("downloadPlaylist");

    addShadowEffect(ui->TitleLbl, Qt::white);

    ui->PosSlider->setStyle(&sliderStyle);

    ui->playlistView->setModel(&playlistModel);
    this->setAcceptDrops(true);

    flick.activateOn(ui->playlistView);

    ui->updateBtn->setVisible(false);

    this->grabGesture(Qt::TapGesture);

    connect(&timer, &QTimer::timeout, this, &MainWindow::timer_event);
    timer.start(100);

    connect(&fastTimer, &QTimer::timeout, this, &MainWindow::fastTimer_event);
    fastTimer.start(1000/30);

    utils->setThumbnailFPS(settings->value("previewFPS", 30).toInt());

    connect(qApp, &QApplication::focusChanged, this, &MainWindow::onFocusChanged);
#ifdef Q_OS_WIN
    bool experimental = globalSettings->value("experimental", false).toBool();
    updater = new Updater(globalSettings->value("currBuild").toInt(), experimental);
    updater->setInstallDir(QApplication::applicationDirPath());
    connect(this, &MainWindow::checkForUpdates, updater, &Updater::checkUpdates);
    connect(updater, &Updater::newUpdates, this, &MainWindow::showUpdate);
    updater->moveToThread(&updaterThread);
    updaterThread.start();
    emit checkForUpdates();

    if(experimental)
    {
        ui->TitleLbl->setText("ModernNewel2Video - wersja testowa");
        ui->TitleLbl->setStyleSheet("padding: 3 0 0 20; color: qlineargradient(spread:pad, x1:0, y1:0, x2:458, y2:0, stop:0 rgba(255, 0, 0, 255),  stop:0.95 rgba(255,0,0,255), stop:1 rgba(0, 0, 0, 0));");
    }
#endif

    QString audioDeviceName = settings->value("audioDeviceName").toString();
    ui->SoundLbl->setText(": " + audioDeviceName);

    videoWindow = new VideoWindow(ui->thumb, settings);
    connect(videoWindow, &VideoWindow::screenNameChanged, this, &MainWindow::setScreenName);
    videoWindow->configurationChanged();

    connect(qApp, &QGuiApplication::screenAdded, this, &MainWindow::screenAdded);
    connect(qApp, &QGuiApplication::screenRemoved, this, &MainWindow::screenRemoved);

    utils->setExtendScreenTopology();

    QString audioDeviceId = settings->value("audioDeviceId").toString();
    WId videoWidgetId = videoWindow->videoWidget->winId();
    bool hwdec = settings->value("hwdec", false).toBool();
    mpv = new MPV(audioDeviceId, videoWidgetId, hwdec);

    connect(mpv, &MPV::processFinished, this, [this](){this->close();});
    connect(mpv, &MPV::eof, this, &MainWindow::mpvEOF);
    connect(mpv, &MPV::currPosChanged, this, &MainWindow::setCurrPos);
    connect(mpv, &MPV::pauseChanged, this, &MainWindow::setPaused);
#ifndef Q_OS_WIN
    connect(mpv, &MPV::peakLevelChanged, this, &MainWindow::setPeakLevel);
#else
    connect(utils, &OSUtils::peakLevelChanged, this, &MainWindow::setPeakLevel);
#endif

    QStringList args = QApplication::arguments();
    if(args.count() <=1 && settings->value("autoPlaylist", false).toBool())
    {
        loadPlaylist();
    }

    if(args.count() > 1)
    {
        playlistModel.addFile(args.at(1));
    }
}

MainWindow::~MainWindow()
{
#ifdef Q_OS_WIN
    updaterThread.quit();
    updaterThread.wait();
#endif

    utils->enableCursorClip(false);

    delete mpv;
    delete videoWindow;
    delete ui;

    delete utils;
}

void MainWindow::loadPlaylist()
{
    QDate currDate = QDate::currentDate();
    QString videoDir = settings->value("videoDir").toString();
    QStringList subdirs = QDir(videoDir).entryList(QDir::Dirs | QDir::NoDotAndDotDot);

    if(settings->value("deleteOldVideos", false).toBool())
    {
        for(int i=0; i<subdirs.size(); i++)
        {
            QDate date = QDate::fromString(subdirs[i], "yyyy-MM-dd");
            qint64 dateDiff = (currDate.toJulianDay()-date.toJulianDay());
            if(date.isValid())
            {
                if((dateDiff >= 7) || (date.dayOfWeek() != 1) && (dateDiff >= 1))
                    QDir(videoDir + "/" + subdirs[i]).removeRecursively();
            }
        }
    }

    QDir dir(videoDir + "/" + currDate.toString("yyyy-MM-dd"));
    if(!dir.exists() && currDate.dayOfWeek() < 6) dir = QDir(videoDir + "/" + currDate.addDays(-(currDate.dayOfWeek()-1)).toString("yyyy-MM-dd"));
    if(!dir.exists()) return;

    QStringList videos = dir.entryList(QDir::Files, QDir::Name);

    for(int i=0; i<videos.size(); i++)
    {
        playlistModel.addFile(dir.path() + "/" + videos[i]);
    }
}

void MainWindow::addShadowEffect(QWidget *widget, QColor color)
{
    QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect(widget);
    effect->setBlurRadius(30);
    effect->setColor(color);
    effect->setOffset(0,0);
    widget->setGraphicsEffect(effect);
}

void MainWindow::calcPeakChannel(int level, QFrame *blue)
{
    const int origHeightBlue = 60;
    const int yBlue = 20;
    int heightBlue = level*5;
    blue->setGeometry(blue->x(), yBlue+(origHeightBlue-heightBlue), blue->width(), heightBlue);
}

void MainWindow::setTitle(QString title)
{
#ifdef Q_OS_WIN
    taskbarButton.progress()->setVisible(!title.isEmpty());
    if(title.isEmpty()) title = globalSettings->value("experimental", false).toBool() ? "ModernNewel2Video - wersja testowa" : "ModernNewel2Video";
#else
    if(title.isEmpty()) title = "ModernNewel2Video";
#endif

    ui->TitleLbl->setText(title);
    this->setWindowTitle(title);
}

void MainWindow::setCurrentPlaylistEntry(int n)
{
    playlistModel.setCurrentItem(n);
    ui->playlistView->setCurrentIndex(playlistModel.index(playlistModel.getCurrentItem(), 0, QModelIndex()));
    ui->playlistView->scrollTo(playlistModel.index(playlistModel.getCurrentItem(), 0, QModelIndex()));
    ui->playlistView->repaint();
}

void MainWindow::playFile(int n)
{
    videoWindow->hideImage();
    videoWindow->hideWindow();

    if(playlistModel.getCurrentItemInfo().type == EntryInfo::Image)
    {
        mpv->stop();
        videoWindow->showImage(playlistModel.getItemInfo(n).filename);
    }
    else if(playlistModel.getCurrentItemInfo().type == EntryInfo::Window)
    {
        mpv->stop();
        videoWindow->showWindow(playlistModel.getItemInfo(n).winID, playlistModel.getItemInfo(n).flags);
    }
    else
    {
        mpv->playFile(playlistModel.getItemInfo(n).filename);
    }
}

void MainWindow::stopFile()
{
    if(playlistModel.getCurrentItemInfo().type == EntryInfo::Image)
    {
        videoWindow->hideImage();
        this->setCurrentPlaylistEntry(playlistModel.getCurrentItem()+1);
    }
    else if(playlistModel.getCurrentItemInfo().type == EntryInfo::Window)
    {
        videoWindow->hideWindow();
        this->setCurrentPlaylistEntry(playlistModel.getCurrentItem()+1);
    }
    else
    {
        mpv->stop();
    }
}

////////////////
//// EVENTS ////
////////////////

bool MainWindow::event(QEvent *event)
{
    QStringList btns;
    btns << "PlayBtn" << "StopBtn" << "AddBtn" << "AddWindowBtn" << "RemoveBtn" << "MoveUpBtn" << "MoveDownBtn" << "playOnce";
    if(event->type() == QEvent::TouchBegin)
    {
        event->accept();
        return true;
    }
    else if(event->type() == QEvent::Gesture)
    {
        QGesture *gesture = ((QGestureEvent*)event)->gesture(Qt::TapGesture);
        QPoint point = ((QTapGesture*)gesture)->hotSpot().toPoint();

        QList<QPushButton*> list = ui->centralWidget->findChildren<QPushButton*>();
        foreach(QPushButton *w, list) {
            if(btns.contains(w->objectName()))
            {
                QRect geom = QRect(w->mapToGlobal(QPoint(0,0)), w->size());
                if(geom.contains(point) && w->isEnabled())
                {
                    w->setStyleSheet("border-color:  rgb(0,0,0); border-bottom-color: rgb(70, 70, 70); border-right-color:  rgb(70, 70, 70); border-width: 4 2 2 4px;");
                }
            }
        }
        return true;
    }
    else if(event->type() == QEvent::TouchEnd)
    {
        QList<QPushButton*> list = ui->centralWidget->findChildren<QPushButton*>();
        foreach(QPushButton *w, list)
        {
            if(btns.contains(w->objectName(), Qt::CaseInsensitive))
            {
                w->setStyleSheet("");
            }
        }

        return true;
    }

    return QWidget::event(event);
}

void MainWindow::showEvent(QShowEvent *e)
{
#ifdef Q_OS_WIN
    taskbarButton.setWindow(windowHandle());
#endif
    e->accept();
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    this->stopFile();

    settings->setValue("windowGeometry", this->saveGeometry());

    videoWindow->canClose = true;
    videoWindow->close();

    utils->setPreviousScreenTopology();

    e->accept();

    qApp->quit();
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    movePos = event->pos();
    canMove = ui->backgroundTitle->geometry().contains(event->pos());
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if ((event->buttons() & Qt::LeftButton) &&
        canMove)
    {
        QPoint diff = event->pos() - movePos;
        QPoint newpos = this->pos() + diff;

        this->move(newpos);
    }
}

void MainWindow::dragEnterEvent(QDragEnterEvent *e)
{
    if(e->mimeData()->hasUrls())
    {
        e->setDropAction(Qt::MoveAction);
        e->accept();
    }
    else
        e->ignore();
}

void MainWindow::dragLeaveEvent(QDragLeaveEvent *e)
{
    e->accept();
}

void MainWindow::dragMoveEvent(QDragMoveEvent *e)
{
    e->setDropAction(Qt::MoveAction);
    e->accept();
}

void MainWindow::dropEvent(QDropEvent *e)
{
    auto urls = e->mimeData()->urls();

    for(int i=0; i<urls.size(); i++)
        playlistModel.addFile(urls[i].toLocalFile());
}

///////////////
//// SLOTS ////
///////////////

void MainWindow::newProcess(const QString &data)
{
    playlistModel.addFile(data);
    this->activateWindow();
}

void MainWindow::timer_event()
{
    if(videoWindow->isVisible() &&
            (!paused || videoWindow->isImageVisible() || videoWindow->isWindowVisible()) &&
            videoWindow->isFullScreen())
        utils->enableCursorClip(true);
    else
        utils->enableCursorClip(false);

    bool hidden = (ui->PosSlider->value() == 0.0) && paused && !videoWindow->isImageVisible() && !videoWindow->isWindowVisible();

    videoWindow->videoWidget->setHidden(hidden);
    if(!settings->value("hideScreen", true).toBool())
    {
        videoWindow->setHidden(hidden);
    }
    else
    {
        videoWindow->setHidden(false);
    }

    this->setTitle(playlistModel.getCurrentItemInfo().title);
}

void MainWindow::fastTimer_event()
{
    static QWidget *widgets[] = {ui->PlayBtn, ui->StopBtn, ui->AddBtn, ui->AddWindowBtn, ui->RemoveBtn, ui->MoveUpBtn, ui->MoveDownBtn, ui->playOnce};
    static int prevColors[8] = {0};
    static int colors[8] = {0};

    for(int i=0; i<8; i++)
    {
        colors[i] = widgets[i]->underMouse() ? qMin(colors[i]+8, 40) : qMax(colors[i]-2, 0);
        if(colors[i] != prevColors[i])
        {
            widgets[i]->setStyleSheet("QPushButton { background-color: rgb(36,36," + QString::number(36+colors[i]) + "); }");
            prevColors[i] = colors[i];
        }
    }
}

void MainWindow::onFocusChanged(QWidget *old, QWidget *now)
{
    Q_UNUSED(old); Q_UNUSED(now);
    if(this->isActiveWindow())
    {
        ui->backgroundTitle->setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1 , stop:0 rgba(20,20,20, 200), stop:0.3 rgba(20,20,20, 228), stop:0.8 rgba(20,20,20, 255));");

        QGraphicsDropShadowEffect *shadow = dynamic_cast<QGraphicsDropShadowEffect*>(ui->TitleLbl->graphicsEffect());
        if(shadow)
        {
            shadow->setBlurRadius(30);
        }
    }
    else
    {
        ui->backgroundTitle->setStyleSheet("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1 , stop:0 rgba(20,20,20, 0), stop:0.3 rgba(20,20,20, 127), stop:0.8 rgba(20,20,20, 255));");

        QGraphicsDropShadowEffect *shadow = dynamic_cast<QGraphicsDropShadowEffect*>(ui->TitleLbl->graphicsEffect());
        if(shadow)
        {
            shadow->setBlurRadius(0);
        }
    }
}

void MainWindow::on_updateBtn_clicked()
{
#ifdef Q_OS_WIN
    UpdateDialog *dlg = new UpdateDialog(updater);
    dlg->setCurr(globalSettings->value("currVersion").toString(), globalSettings->value("currBuild").toInt());
    dlg->setNew(newVersion, newBuild);
    dlg->setAttribute(Qt::WA_DeleteOnClose);
    dlg->exec();
#endif
}

void MainWindow::on_settingsBtn_clicked()
{
    Settings *set = new Settings(settings);
    set->setAttribute(Qt::WA_DeleteOnClose);
    set->exec();
    videoWindow->configurationChanged();
    utils->setThumbnailFPS(settings->value("previewFPS", 30).toInt());
}

void MainWindow::on_minimizeBtn_clicked()
{
    this->showMinimized();
}

void MainWindow::on_exitBtn_clicked()
{
    this->close();
}

void MainWindow::on_PlayBtn_clicked()
{
    int currItem = ui->playlistView->currentIndex().row();
    if(currItem < 0) currItem = playlistModel.getCurrentItem();
    if(currItem >= playlistModel.rowCount(QModelIndex())) currItem = 0;

    if(playlistModel.getItemInfo(currItem).type == EntryInfo::Empty)
        return;

    if(paused && (ui->PosSlider->value() == 0))
    {
        this->setCurrentPlaylistEntry(currItem);
        this->playFile(currItem);
        return;
    }

    mpv->pause();
}

void MainWindow::on_StopBtn_clicked()
{
    this->stopFile();
}

void MainWindow::on_AddBtn_clicked()
{
    QString imgFilter;
    foreach (QByteArray ext, QImageReader::supportedImageFormats())
    {
        imgFilter += "*." + ext + " ";
    }
    imgFilter = imgFilter.left(imgFilter.length()-1);

    QStringList filenames = QFileDialog::getOpenFileNames(0,
                                                    "Wybierz pliki wideo",
                                                    settings->value("lastDir").toString(),
                                                    "Wszystkie obsługiwane pliki (*.rmvb *.mpeg *.mpg *.mpe *.m1v *.m2v *.mp4 *.mp4v *.mpg4 *.avi *.ogv *.qt *.mov *.asf *.asx *.wmv *.wmx *.fli *.flv *.mkv *.mk3d *.mks *.webm *.aac *.mpga *.mp2 *.mp2a *.mp3 *.m2a *.m3a *.oga *.ogg *.spx *.ram *.ra *.rmp *.wma *.wav *.flac *.m4a *.mp4a " + imgFilter + ");;\
                                                          Pliki wideo (*.rmvb *.mpeg *.mpg *.mpe *.m1v *.m2v *.mp4 *.mp4v *.mpg4 *.avi *.ogv *.qt *.mov *.asf *.asx *.wmv *.wmx *.fli *.flv *.mkv *.mk3d *.mks *.webm);;\
                                                          Pliki audio (*.aac *.mpga *.mp2 *.mp2a *.mp3 *.m2a *.m3a *.oga *.ogg *.spx *.ram *.ra *.rmp *.wma *.wav *.flac *.m4a *.mp4a);;\
                                                          Pliki graficzne (" + imgFilter + ");;\
                                                          Wszystkie pliki (*.*)");

    if(!filenames.isEmpty())
    {
        settings->setValue("lastDir", QFileInfo(filenames[0]).dir().absolutePath());

        for(int i=0; i<filenames.size(); i++)
            playlistModel.addFile(filenames[i]);
    }
}

void MainWindow::on_AddWindowBtn_clicked()
{
    WindowSelector *selector = new WindowSelector(this);
    selector->setAttribute(Qt::WA_DeleteOnClose);
    connect(selector, &WindowSelector::windowSelected, this, [this, selector](WId windowID, int flags) {
        playlistModel.addWindow(windowID, flags);
        selector->close();
    });

    selector->show();
}

void MainWindow::on_RemoveBtn_clicked()
{
    if((ui->playlistView->currentIndex().row() == playlistModel.getCurrentItem()) &&
            (playlistModel.getCurrentItemInfo().type == EntryInfo::AV))
        this->stopFile();

    if((ui->playlistView->currentIndex().row() == playlistModel.getCurrentItem()) &&
            (playlistModel.getCurrentItemInfo().type == EntryInfo::Image))
        videoWindow->hideImage();

    if((ui->playlistView->currentIndex().row() == playlistModel.getCurrentItem()) &&
            (playlistModel.getCurrentItemInfo().type == EntryInfo::Window))
        videoWindow->hideWindow();

    playlistModel.removeEntry(ui->playlistView->currentIndex().row());
}

void MainWindow::on_playOnce_clicked()
{
    if(playOnce)
    {
        ui->playOnce->setIcon(QIcon(":/images/nonstop.png"));
        ui->playOnce->setToolTip("Odtwarzanie plików bez zatrzymywania");
    }
    else
    {
        ui->playOnce->setIcon(QIcon(":/images/once.png"));
        ui->playOnce->setToolTip("Odtwarzanie plików pojedynczo");
    }

    playOnce = !playOnce;
}

void MainWindow::on_MoveUpBtn_clicked()
{
    int currRow = ui->playlistView->currentIndex().row();
    if(currRow < 1) return;

    playlistModel.swapEntries(currRow, currRow-1);
    QModelIndex idx = playlistModel.index(currRow-1, 0, QModelIndex());
    ui->playlistView->setCurrentIndex(idx);
}

void MainWindow::on_MoveDownBtn_clicked()
{
    int currRow = ui->playlistView->currentIndex().row();
    if(currRow >= playlistModel.rowCount(QModelIndex())-1) return;

    playlistModel.swapEntries(currRow, currRow+1);
    QModelIndex idx = playlistModel.index(currRow+1, 0, QModelIndex());
    ui->playlistView->setCurrentIndex(idx);
}

void MainWindow::on_playlistView_doubleClicked(const QModelIndex &index)
{
    videoWindow->hideImage();
    videoWindow->hideWindow();

    if(playlistModel.getCurrentItemInfo().type == EntryInfo::AV)
    {
        this->stopFile();
    }

    this->setCurrentPlaylistEntry(index.row());
}

void MainWindow::on_PosSlider_valueChanged(int value)
{
   QTime cp = QTime(0,0,0).addSecs(value/10);
   QTime tp = QTime(0,0,0).addSecs(playlistModel.getCurrentItemInfo().duration);
   ui->PosLbl->setText(cp.toString("H:mm:ss") + "/" + tp.toString("H:mm:ss"));
}

void MainWindow::on_PosSlider_sliderReleased()
{
    mpv->seek(ui->PosSlider->value()/10);
}

void MainWindow::showUpdate(QString version, int build)
{
#ifdef Q_OS_WIN
    newVersion = version;
    newBuild = build;
    ui->updateBtn->setVisible(true);

    ui->TitleLbl->resize(438, ui->TitleLbl->height());
    ui->TitleLbl->setStyleSheet("padding: 3 0 0 20; "
                                "color: qlineargradient(spread:pad, x1:0, y1:0, x2:428, y2:0, stop:0 rgba(255, 255, 255, 255),  stop:0.95 rgba(255,255,255,255), stop:1 rgba(0, 0, 0, 0));");
#endif
}

void MainWindow::setPaused(bool paused)
{
    this->paused = paused;
#ifdef Q_OS_WIN
    taskbarButton.progress()->setPaused(paused);
#endif
    ui->PlayBtn->setIcon(QIcon(paused ? ":/images/play.png" : ":/images/pause.png"));
    ui->PlayBtn->setToolTip(paused ? "Odtwarzaj" : "Pauza");
}

void MainWindow::mpvEOF()
{
    this->setCurrentPlaylistEntry(playlistModel.getCurrentItem()+1);

    if(playOnce || playlistModel.getCurrentItemInfo().type == EntryInfo::Empty)
    {
        mpv->setPause(true);
    }
    else
    {
        this->playFile(playlistModel.getCurrentItem());
    }
}

void MainWindow::setCurrPos(double currPos)
{
    int totalPos = playlistModel.getCurrentItemInfo().duration;

    if(!ui->PosSlider->isSliderDown())
    {
        ui->PosSlider->setMinimum(0);
        ui->PosSlider->setMaximum(totalPos*10);
        ui->PosSlider->setValue(currPos*10);

        QTime cp = QTime(0,0,0).addSecs(currPos);
        QTime tp = QTime(0,0,0).addSecs(totalPos);
        ui->PosLbl->setText(cp.toString("H:mm:ss") + "/" + tp.toString("H:mm:ss"));
    }
#ifdef Q_OS_WIN
    taskbarButton.progress()->setMinimum(0);
    taskbarButton.progress()->setMaximum(totalPos ? totalPos*10 : 1);
    taskbarButton.progress()->setValue(currPos*10);
#endif

    static bool done = false;
    if(currPos != 0.0f && !done)
    {
        utils->initPeakMeter(mpv->getPID());
        done = true;
    }
}

void MainWindow::setPeakLevel(double peakLevel)
{
    if(paused) peakLevel = 0.0;

    static int maxPeak = 0;
    maxPeak = qMax<int>(maxPeak, peakLevel*12);
    calcPeakChannel(maxPeak, ui->peak);
    maxPeak = qMax<int>(maxPeak-1, 0);
}

void MainWindow::screenAdded(QScreen *screen)
{
    videoWindow->configurationChanged();

    connect(screen, &QScreen::geometryChanged, videoWindow, [this](){videoWindow->configurationChanged();});
}

void MainWindow::screenRemoved(QScreen *screen)
{
    videoWindow->configurationChanged();

    disconnect(screen, &QScreen::geometryChanged, videoWindow, 0);
}

void MainWindow::setScreenName(QString name)
{
    ui->ScreenLbl->setText(": " + name);
}
