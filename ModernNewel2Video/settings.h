/*
    ModernNewel2Video
    Copyright (C) 2015-2016  Lukasz Matczak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QSettings>
#include <QAbstractButton>
#include <QSpinBox>

struct AudioInfo
{
    QString id;
    QString name;

    AudioInfo()
    {

    }

    AudioInfo(QSettings *settings)
    {
        id = settings->value("audioDeviceId").toString();
        name = settings->value("audioDeviceName").toString();
    }

    void save(QSettings *settings)
    {
        settings->setValue("audioDeviceId", id);
        settings->setValue("audioDeviceName", name);
    }

    QString toString()
    {
        return name;
    }

    inline bool operator==(const AudioInfo& x)
    {
        return (!x.id.isEmpty() && (x.id == this->id));
    }
};
Q_DECLARE_METATYPE(AudioInfo)

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(QSettings *set, QWidget *parent = 0);
    ~Settings();

    static QVector<AudioInfo> ainfo;

private slots:
    void on_buttonBox_clicked(QAbstractButton *button);
    void on_setAssocBtn_clicked();
    void on_autoPlaylistChk_stateChanged(int arg1);
    void on_videoDirBtn_clicked();
    void on_hideScreenChk_stateChanged(int arg1);

private:
    void drawMonitors();
    void setScreenLbl();
    void resizeEvent(QResizeEvent *event);

    Ui::Settings *ui;

    QSettings *settings;

    QRect selectedMonitor;
#ifdef Q_OS_LINUX
    QSpinBox *fpsSpin;
#endif
};

#endif // SETTINGS_H
