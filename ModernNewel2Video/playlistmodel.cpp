/*
    ModernNewel2Video
    Copyright (C) 2015-2016  Lukasz Matczak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "playlistmodel.h"
#include "mainwindow.h"

#include <QFileInfo>
#include <QDir>
#include <QFont>
#include <QJsonArray>
#include <QDebug>
#include <QImageReader>

#include <taglib/tag.h>
#include <taglib/fileref.h>

PlaylistModel::PlaylistModel()
{
    this->currItem = 0;

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &PlaylistModel::updateEntries);
    timer->start(1000);
}

PlaylistModel::~PlaylistModel()
{

}

QModelIndex PlaylistModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return createIndex(row, column);
}

QModelIndex PlaylistModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child);
    return QModelIndex();
}

int PlaylistModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return entries.size();
}

int PlaylistModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 1;
}

QVariant PlaylistModel::data(const QModelIndex &index, int role) const
{
    if(role == Qt::DisplayRole)
    {
        return entries[index.row()].title;
    }
    else if(role == Qt::ToolTipRole)
    {
        QString tooltip = entries[index.row()].title;
        if(!entries[index.row()].filename.isEmpty())
        {
            tooltip += "\n" + QDir::toNativeSeparators(entries[index.row()].filename);
        }

        int flags = entries[index.row()].flags;
        if(flags) tooltip += "\nOpcje:";
        if(flags & EF_WIN_SCALE)      tooltip += "\n  Skalowanie";
        if(flags & EF_WIN_WITHCURSOR) tooltip += "\n  Pokazywanie kursora myszy";

        return tooltip;
    }
    else if(role == Qt::FontRole)
    {
        QFont font;
        if(index.row() == currItem) font.setBold(true);
        if(!entries[index.row()].exists) font.setStrikeOut(true);
        return font;
    }
    return QVariant();
}

void PlaylistModel::addFile(QString filename)
{
    emit layoutAboutToBeChanged();

    EntryInfo entry;
    entry.filename = filename;
    entry.type = EntryInfo::AV;
#ifdef Q_OS_WIN
    TagLib::FileRef filetag(filename.toStdWString().c_str());
#else
    TagLib::FileRef filetag(filename.toStdString().c_str());
#endif
    TagLib::Tag *tag = filetag.tag();
#ifdef Q_OS_WIN
    if(tag) entry.title = QString::fromUtf16((ushort*)tag->title().toCWString());
#else
    if(tag) entry.title = QString::fromUtf8(tag->title().toCString(true));
#endif
    if(entry.title.isEmpty()) entry.title = QFileInfo(filename).fileName();

    TagLib::AudioProperties *audioProperties = filetag.audioProperties();
    if(audioProperties) entry.duration = audioProperties->length();
    entry.exists = QFileInfo(filename).exists();

    foreach (QByteArray ext, QImageReader::supportedImageFormats())
    {
        if(filename.endsWith("."+ext, Qt::CaseInsensitive))
            entry.type = EntryInfo::Image;
    }

    entries.push_back(entry);

    emit layoutChanged();
    emit dataChanged(createIndex(entries.size()-1, 0), createIndex(entries.size()-1, 0)); // TODO: ???
}

void PlaylistModel::addWindow(WId winID, int flags)
{
    emit layoutAboutToBeChanged();

    EntryInfo entry;
    entry.type = EntryInfo::Window;
    entry.exists = true;
    entry.winID = winID;
    entry.flags = flags;
    entry.title = "Okno \"" + utils->getWindowTitle(winID) + "\"";

    entries.push_back(entry);

    emit layoutChanged();
    emit dataChanged(createIndex(entries.size()-1, 0), createIndex(entries.size()-1, 0)); // TODO: ???
}

void PlaylistModel::removeEntry(int pos)
{
    emit layoutAboutToBeChanged();

    if(!(pos < 0 || pos >= entries.size()))
        entries.remove(pos);

    emit layoutChanged();
}

void PlaylistModel::swapEntries(int pos1, int pos2)
{
    qSwap(entries[pos1], entries[pos2]);

    if(this->getCurrentItem() == pos1)
        this->setCurrentItem(pos2);
    else if(this->getCurrentItem() == pos2)
        this->setCurrentItem(pos1);
}

EntryInfo PlaylistModel::getItemInfo(int n)
{
    if(n < 0 || n >= entries.size())
        return EntryInfo();

    return entries[n];
}

EntryInfo PlaylistModel::getCurrentItemInfo()
{
    return getItemInfo(this->currItem);
}

void PlaylistModel::setCurrentItem(int n)
{
    emit layoutAboutToBeChanged();
    int prev = this->currItem;

    this->currItem = n;

    emit dataChanged(createIndex(prev, 0), createIndex(prev, 0));
    emit dataChanged(createIndex(n, 0), createIndex(n, 0));

    emit layoutChanged();
}

int PlaylistModel::getCurrentItem()
{
    return this->currItem;
}

void PlaylistModel::updateEntries()
{
    for(int i=0; i<entries.size(); i++)
    {
        if(entries[i].type == EntryInfo::Window)
        {
            QString newTitle = utils->getWindowTitle(entries[i].winID);
            if(newTitle.isNull())
            {
                entries[i].exists = false;
            }
            else
            {
                entries[i].title = "Okno \"" + newTitle + "\"";
            }

            emit dataChanged(createIndex(i, 0), createIndex(i, 0));
        }
    }
}
