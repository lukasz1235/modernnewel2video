/*
    ModernNewel2Video
    Copyright (C) 2016  Lukasz Matczak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "windowselector.h"

#include <QFrame>
#include <QGuiApplication>
#include <QScreen>
#include <QGraphicsDropShadowEffect>
#include <QSequentialAnimationGroup>
#include <QPropertyAnimation>
#include <QCheckBox>
#include <QPushButton>
#include <QGestureEvent>
#include <QDebug>

bool WindowSelector::scaleChkState = false;
bool WindowSelector::mouseChkState = false;

WindowSelector::WindowSelector(MainWindow *mainWindow, QWidget *parent) : QWidget(parent), mainWindow(mainWindow)
{
    this->selected = false;
    this->prevThumb = -1;

    this->setStyleSheet("\
QWidget { \
    background: transparent; \
    color: white; \
    font: 14pt \"Open Sans Light\"; \
} \
QPushButton { \
    border-color: rgb(0,0,0); \
    border-top-color: rgb(70, 70, 70); \
    border-left-color: rgb(70, 70, 70); \
    border-width : 2 4 4 2px; \
    border-style: solid; \
    border-radius: 10px; \
} \
QPushButton:hover { \
    background-color: rgb(36,36,76); \
} \
QPushButton:pressed { \
    border-color: rgb(0,0,0); \
    border-bottom-color: rgb(70, 70, 70); \
    border-right-color: rgb(70, 70, 70); \
    border-width: 4 2 2 4px; \
} \
QCheckBox::indicator { \
    width: 16px; \
    height: 16px; \
} \
QCheckBox::indicator:checked         { image: url(:/images/checkbox_checked.png); } \
QCheckBox::indicator:checked:hover   { image: url(:/images/checkbox_checked_hover.png); } \
QCheckBox::indicator:unchecked       { image: url(:/images/checkbox_unchecked.png); } \
QCheckBox::indicator:unchecked:hover { image: url(:/images/checkbox_unchecked_hover.png); } \
QCheckBox::indicator:disabled        { image: url(:/images/checkbox_disabled.png); } \
QCheckBox:disabled                   { color: rgb(127,127,127); }");
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setAttribute(Qt::WA_AcceptTouchEvents);
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::ToolTip);
    this->setWindowTitle("ModernNewel2Video_WindowSelector");
    this->setCursor(Qt::CrossCursor);

    areaFrm = new QFrame(this);
    areaFrm->setStyleSheet("background-color: rgba(127,127,127,2)");
    areaFrm->move(0, 0);
    areaFrm->show();

    QRect geom = QGuiApplication::primaryScreen()->geometry();
    this->setGeometry(geom);
    areaFrm->resize(geom.size());

    borderFrm = new QFrame(this);
    borderFrm->setStyleSheet("background-color: rgba(127,127,255,64); border: 4px solid #8080ff;");
    borderFrm->setGeometry(0,0,0,0);

    infoLbl = new QLabel("Wybierz okno, którego zawartość\nma być sklonowana na drugi ekran.\n\nAby anulować naciśnij ESC.", this);
    infoLbl->setObjectName("infoLbl");
    infoLbl->resize(330, 130);
    infoLbl->move((geom.width()-infoLbl->width())/2, (geom.height()-infoLbl->height())/2);
    infoLbl->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    infoLbl->setStyleSheet("#infoLbl { background-color: rgba(20,20,20,200); border: 2px solid black; border-radius: 10px; }");

    QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect(infoLbl);
    effect->setBlurRadius(30);
    effect->setColor(Qt::black);
    effect->setOffset(0,0);
    infoLbl->setGraphicsEffect(effect);

    infoLbl->show();

    this->setMouseTracking(true);
    areaFrm->setMouseTracking(true);
    borderFrm->setMouseTracking(true);
    infoLbl->setMouseTracking(true);

    connect(&updateTimer, &QTimer::timeout, this, &WindowSelector::update);
    updateTimer.start(1000/30);
    hoverWindow = 0;

    this->grabKeyboard();
}

WindowSelector::~WindowSelector()
{
    utils->destroyThumbnail(this->prevThumb);
    this->releaseKeyboard();
}

void WindowSelector::update()
{
    if(hoverWindow)
        borderFrm->setGeometry(utils->getWindowRect(hoverWindow).marginsAdded(QMargins(2,2,2,2)));
}

bool WindowSelector::event(QEvent *event)
{
    QStringList btns;
    btns << "okBtn" << "cancelBtn";
    if(event->type() == QEvent::TouchBegin)
    {
        event->accept();
        return true;
    }
    else if(event->type() == QEvent::Gesture)
    {
        QGesture *gesture = ((QGestureEvent*)event)->gesture(Qt::TapGesture);
        QPoint point = ((QTapGesture*)gesture)->hotSpot().toPoint();

        QList<QPushButton*> list = this->findChildren<QPushButton*>();
        foreach(QPushButton *w, list) {
            if(btns.contains(w->objectName()))
            {
                QRect geom = QRect(w->mapToGlobal(QPoint(0,0)), w->size());
                if(geom.contains(point) && w->isEnabled())
                {
                    w->setStyleSheet("border-color: rgb(0,0,0); border-bottom-color: rgb(70, 70, 70); border-right-color: rgb(70, 70, 70); border-width: 4 2 2 4px;");
                }
            }
        }
        return true;
    }
    else if(event->type() == QEvent::TouchEnd)
    {
        QList<QPushButton*> list = this->findChildren<QPushButton*>();
        foreach(QPushButton *w, list)
        {
            if(btns.contains(w->objectName(), Qt::CaseInsensitive))
            {
                w->setStyleSheet("");
            }
        }

        return true;
    }

    return QWidget::event(event);
}

void WindowSelector::mouseMoveEvent(QMouseEvent *e)
{
    if(!selected)
    {
        WindowInfo wi = utils->getWindowAt(e->pos(), this->winId());

        if((wi.windowID == mainWindow->winId()) || (wi.windowID == mainWindow->videoWindow->winId()))
        {
            borderFrm->setStyleSheet("background-color: rgba(127,127,127,64); border: 4px solid #808080;");
        }
        else
        {
            borderFrm->setStyleSheet("background-color: rgba(127,127,255,64); border: 4px solid #8080ff;");
        }

        borderFrm->setGeometry(wi.geometry.marginsAdded(QMargins(2,2,2,2)));
        hoverWindow = wi.windowID;
    }
}

void WindowSelector::mousePressEvent(QMouseEvent *e)
{
    if(e->button() == Qt::LeftButton && !this->selected)
    {
        WindowInfo wi = utils->getWindowAt(e->pos(), this->winId());

        if((wi.windowID == mainWindow->winId()) || (wi.windowID == mainWindow->videoWindow->winId()))
            return;

        this->selected = true;

        QSequentialAnimationGroup *animGroup = new QSequentialAnimationGroup(this);
        const int destWidth = 570, destHeight = 410;

        QPropertyAnimation *widthAnim = new QPropertyAnimation(infoLbl, "geometry", this);
        widthAnim->setDuration((destWidth-infoLbl->width())*2);
        widthAnim->setStartValue(infoLbl->geometry());
        widthAnim->setEndValue(QRect(infoLbl->x()-(destWidth-infoLbl->width())/2,
                                     infoLbl->y(),
                                     destWidth, infoLbl->height()));
        widthAnim->setEasingCurve(QEasingCurve::InOutCubic);
        animGroup->addAnimation(widthAnim);

        QPropertyAnimation *heightAnim = new QPropertyAnimation(infoLbl, "geometry", this);
        heightAnim->setDuration((destHeight-infoLbl->height())*2);
        heightAnim->setStartValue(widthAnim->endValue());
        heightAnim->setEndValue(QRect(infoLbl->x()-(destWidth-infoLbl->width())/2,
                                      infoLbl->y()-(destHeight-infoLbl->height())/2,
                                      destWidth, destHeight));
        heightAnim->setEasingCurve(QEasingCurve::InOutCubic);
        animGroup->addAnimation(heightAnim);

        animGroup->start();

        QEventLoop loop;
        connect(animGroup, &QSequentialAnimationGroup::finished, &loop, &QEventLoop::quit);
        loop.exec();

        infoLbl->setText("");

        QLabel *previewBg = new QLabel(infoLbl);
        previewBg->resize(mainWindow->videoWindow->size().scaled(530, 225, Qt::KeepAspectRatio));
        previewBg->move((destWidth-previewBg->width())/2, 15);
        previewBg->setStyleSheet("background-color: black;");
        previewBg->setAlignment(Qt::AlignCenter | Qt::AlignHCenter);
        previewBg->show();

        prevThumb = utils->createThumbnail(wi.windowID, previewBg, true, false);
        float thumbScale = qMin((float)previewBg->width()/mainWindow->videoWindow->width(),
                          (float)previewBg->height()/mainWindow->videoWindow->height());
        utils->setThumbnailScale(prevThumb, thumbScale);
        utils->moveThumbnail(prevThumb, QSize());

        QCheckBox *scaleChk = new QCheckBox("Skaluj obraz okna do rozmiaru ekranu", infoLbl);
        connect(scaleChk, &QCheckBox::stateChanged, this, [this, previewBg](int state) {
            float thumbScale;
            if(state == Qt::Checked)
            {
                thumbScale = -1.0f;
            }
            else
            {
                thumbScale = qMin((float)previewBg->width()/mainWindow->videoWindow->width(),
                                  (float)previewBg->height()/mainWindow->videoWindow->height());
            }

            utils->setThumbnailScale(prevThumb, thumbScale);
            utils->moveThumbnail(prevThumb, QSize());
        });
        scaleChk->move(20, 250);
        scaleChk->setChecked(this->scaleChkState);
        scaleChk->show();

        QCheckBox *adjustChk = new QCheckBox(QString("Dostosuj rozmiar okna do rozdzielczości ekranu (%1x%2)")
                                             .arg(mainWindow->videoWindow->width())
                                             .arg(mainWindow->videoWindow->height()),
                                             infoLbl);
        if(mainWindow->videoWindow->width() > QGuiApplication::primaryScreen()->availableGeometry().width() ||
                mainWindow->videoWindow->height() > QGuiApplication::primaryScreen()->availableGeometry().height())
            adjustChk->setEnabled(false);
        connect(adjustChk, &QCheckBox::stateChanged, this, [this, wi](int state) {
            static QSize oldSize = utils->getWindowRect(wi.windowID).size();

            if(state == Qt::Checked)
            {
                utils->setWindowSize(wi.windowID, mainWindow->videoWindow->size());
            }
            else
            {
                utils->setWindowSize(wi.windowID, oldSize);
            }

            borderFrm->setGeometry(utils->getWindowRect(wi.windowID).marginsAdded(QMargins(2,2,2,2)));
            hoverWindow = wi.windowID;
        });
        adjustChk->move(20, 280);
        adjustChk->show();

        QCheckBox *mouseChk = new QCheckBox("Pokazuj kursor myszy", infoLbl);
        mouseChk->move(20, 310);
        mouseChk->setChecked(this->mouseChkState);
        mouseChk->show();

        QPushButton *okBtn = new QPushButton("OK", infoLbl);
        okBtn->setObjectName("okBtn");
        connect(okBtn, &QPushButton::clicked, this, [this, scaleChk, adjustChk, mouseChk, wi]() {
            this->scaleChkState = scaleChk->isChecked();
            this->mouseChkState = mouseChk->isChecked();

            int flags = 0;
            flags |= scaleChk->isChecked() * EF_WIN_SCALE;
            flags |= mouseChk->isChecked() * EF_WIN_WITHCURSOR;
            emit windowSelected(wi.windowID, flags);
        });
        okBtn->setGeometry(295, 350, 120, 40);
        okBtn->show();

        QPushButton *cancelBtn = new QPushButton("Anuluj", infoLbl);
        cancelBtn->setObjectName("cancelBtn");
        connect(cancelBtn, &QPushButton::clicked, this, [this]() {
            this->close();
        });
        cancelBtn->setGeometry(430, 350, 120, 40);
        cancelBtn->show();

        this->setCursor(Qt::ArrowCursor);
    }
}

void WindowSelector::keyReleaseEvent(QKeyEvent *e)
{
    if(e->key() == Qt::Key_Escape)
        this->close();
}
